import * as path from "path";
import { DefinePlugin, Configuration } from "webpack";
import ExtractTextPlugin from "extract-text-webpack-plugin";
import VueLoaderPlugin from "vue-loader/lib/plugin";
import HtmlPlugin from "html-webpack-plugin";
import TerserPlugin from "terser-webpack-plugin";

const TARGET = require("./configs/target")[process.env.NODE_ENV];

const config: Configuration = {
	entry: {
		app: "./src/bootstrap.app.ts",
		login: "./src/bootstrap.login.ts"
	},
	resolve: {
		extensions: [".ts", ".tsx", ".js", ".vue"],
		alias: {
			"@": path.resolve(__dirname, "src/app"),
			"@login": path.resolve(__dirname, "src/login"),
			"@modules": path.resolve(__dirname, "src/modules"),
			"@api": path.resolve(__dirname, "src/api"),
			"@i18n": path.resolve(__dirname, "src/i18n"),
			"@theme": path.resolve(__dirname, "src/theme"),
			"@configs": path.resolve(__dirname, "src/configs"),
			"vue$": "vue/dist/vue.esm.js"
		}
	},
	output: {
		path: TARGET.output.path,
		filename: "js/[name].bundle.js",
		publicPath: TARGET.output.publicPath
	},
	devServer: {
		host: "0.0.0.0",
		proxy: {
			"/": "http://176.111.61.89:5000"
		}
	},
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	mode: process.env.NODE_ENV as any,
	devtool:
		process.env.NODE_ENV == "development"
			? "cheap-module-source-map"
			: false,
	module: {
		rules: [
			{
				enforce: "pre",
				test: /\.(js|ts|tsx)$/,
				exclude: /node_modules/,
				loader: "eslint-loader"
			},
			{
				test: /\.tsx$/,
				use: [
					{
						loader: "babel-loader",
						options: {
							presets: ["@vue/babel-preset-jsx"]
						}
					},
					{
						loader: "ts-loader"
					}
				]
			},
			{
				test: /\.ts$/,
				loader: "ts-loader"
			},
			{
				test: /\.vue$/,
				loader: "vue-loader"
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					fallback: "style-loader",
					use: ["css-loader"]
				})
			},
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					fallback: "style-loader",
					use: ["css-loader", "sass-loader"]
				})
			},
			{
				test: /\.(eot|svg|ttf|woff|woff2)$/,
				use: [
					{
						loader: "file-loader",
						options: {
							outputPath: "fonts"
						}
					}
				]
			},
			{
				test: /\.html$/,
				use: [
					{
						loader: "html-loader",
						options: {
							minimize: true,
							caseSensitive: true
						}
					}
				]
			},
			{
				test: /\.(png|jpg|gif)$/,
				use: [
					{
						loader: "url-loader",
						options: {
							limit: 1,
							name: "images/[hash].[ext]"
						}
					}
				]
			},
			{
				test: /\.font\.(js|json)$/,
				loader: ExtractTextPlugin.extract({
					fallback: "style-loader",
					use: [
						{
							loader: "css-loader",
							options: { url: false }
						},
						{
							loader: "webfonts-loader",
							options: {
								fileName: "fonts/[fontname].[hash].[ext]"
							}
						}
					]
				})
			}
		]
	},
	optimization: {
		minimize: true,
		minimizer: [
			new TerserPlugin({
				extractComments: {
					condition: false
				}
			})
		]
	},
	plugins: [
		new HtmlPlugin({
			title: "Smart Store - CMS",
			chunks: ["app"],
			template: path.resolve(__dirname, "./public/index.ejs"),
			filename: path.resolve(TARGET.output.path, "./index.html")
		}),
		new HtmlPlugin({
			title: "Smart Store - Login",
			chunks: ["login"],
			template: path.resolve(__dirname, "./public/login.ejs"),
			filename: path.resolve(TARGET.output.path, "./login.html")
		}),
		new ExtractTextPlugin("css/[name].styles.css"),
		new DefinePlugin({
			"process.env": JSON.stringify({
				...TARGET.env,
				NODE_ENV: JSON.stringify(process.env.NODE_ENV)
			})
		}),
		new VueLoaderPlugin()
	]
};

export default config;
