import common from "./common";
import buttons from "./buttons";
import pages from "./pages";
import forms from "./forms";
import messages from "./messages";

export const translations = {
	...common,
	buttons,
	pages,
	forms,
	messages
};
