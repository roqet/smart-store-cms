export default {
	add: "Додати",
	save: "Зберегти",
	cancel: "Скасувати",
	edit: "Редагувати",
	sendSMS: "Відправити SMS",
	logIn: "Увійти",
	logOut: "Вийти"
};
