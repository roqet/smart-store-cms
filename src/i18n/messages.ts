export default {
	success: {
		cityAdded: 'Місто "{name}" успішно додано.',
		newsCreated: 'Новина "{title}" успішно створена.',
		newsUpdated: 'Новина "{title}" успішно оновлена.',
		shopCreated: 'Магазин за дресою "{address}" успішно створено.',
		shopUpdated: 'Магазин за дресою "{address}" успішно оновлено.',
		contactsUpdated: "Контакти успішно оновлено.",
		aboutUpdated: 'Інформацію "Про Нас" успішно оновлено.',
		pricesUpdated: "Ціни успішно оновлено."
	}
};
