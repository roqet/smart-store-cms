import Vue from "vue";
import VueToasted, { ToastAction } from "vue-toasted";
import { LocaleMessage } from "vue-i18n";

Vue.use(VueToasted);

interface Toast {
	[key: string]: (
		message: string | LocaleMessage,
		actions?: ToastAction[]
	) => void;
}

declare module "vue/types/vue" {
	interface Vue {
		$toast: {
			success: (
				message: string | LocaleMessage,
				actions?: ToastAction[]
			) => void;
			error: (
				message: string | LocaleMessage,
				actions?: ToastAction[]
			) => void;
		};
	}
}

export class RctToastPlugin {
	public static install(): void {
		Vue.prototype.$toast = [
			{
				// eslint-disable-next-line @typescript-eslint/no-explicit-any
				icon: "check" as any,
				type: "success"
			},
			{
				// eslint-disable-next-line @typescript-eslint/no-explicit-any
				icon: "error_outline" as any,
				type: "error"
			}
		].reduce((res: object, item): Toast => {
			return {
				...res,
				[item.type]: (
					message: string,
					actions: ToastAction[] = []
				): void => {
					Vue.toasted.show(message, {
						...item,
						duration: 5000,
						position: "bottom-right",
						action: [
							...actions,
							{
								text: "Close",
								onClick: (e, toastObject): void =>
									toastObject.goAway(0)
							}
						]
					});
				}
			};
		}, {});
	}
}
