import * as Rules from "./validation.rules";
import * as Messages from "./validation.messages";
import { ValidationRules } from "./validation.rules";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Value = any;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Param = any;

export class ValidationService {
	public static validateFunction(
		expression: Function,
		value: Value,
		param: Param
	): Promise<boolean> {
		return Promise.resolve(expression(value, param)).then(
			(isvalid): Promise<boolean> => {
				if (isvalid) return Promise.resolve(isvalid);
				else return Promise.reject();
			}
		);
	}

	public static validateRegexp(
		expression: RegExp,
		value: Value
	): Promise<boolean> {
		if (value) {
			return Promise.resolve(expression.test(value)).then(
				(isvalid): Promise<boolean> => {
					if (isvalid) return Promise.resolve(isvalid);
					else return Promise.reject(isvalid);
				}
			);
		}
		return Promise.resolve(true);
	}

	public static excludeValidationRule(
		rule: string,
		validation: ValidationRules
	): ValidationRules {
		return Object.keys(validation)
			.filter((key): boolean => key != rule)
			.reduce((res, key): ValidationRules => {
				return { ...res, [key]: validation[key] };
			}, {});
	}

	public static validateRule(
		value: Value,
		key: string,
		rest: ValidationRules = {}
	): Promise<boolean | string> {
		return ((
			expression,
			key: string,
			rest: ValidationRules
		): Promise<boolean | string> => {
			if (expression.constructor == Function) {
				return this.validateFunction(
					expression,
					value,
					rest[key]
				).catch(
					(): Promise<string> =>
						Promise.reject({ [key]: Messages[key].error })
				);
			}
			if (expression.constructor === RegExp) {
				return this.validateRegexp(expression, value);
			}
			return Promise.resolve(true);
		})(Rules[key], key, rest)
			.then(
				(): ValidationRules => {
					return this.excludeValidationRule(key, rest);
				}
			)
			.then(
				(validation): Promise<boolean | string> => {
					if (Object.keys(validation).length) {
						return this.validateRule(
							value,
							Object.keys(validation)[0],
							validation
						);
					} else return Promise.resolve(true);
				}
			)
			.catch(
				(error): Promise<string> => {
					return Promise.reject(error);
				}
			);
	}

	public static validate(value, validation): Promise<boolean | string> {
		if (Object.keys(validation).length) {
			return Promise.resolve(validation).then(
				(validation): Promise<boolean | string> => {
					return this.validateRule(
						value,
						Object.keys(validation)[0],
						validation
					);
				}
			);
		}
		return Promise.resolve(true);
	}
}
