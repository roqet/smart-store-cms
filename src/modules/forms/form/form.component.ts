import { ValidationObserver } from "vee-validate";
import { Component, Mixins, Prop, Emit } from "vue-property-decorator";
import RctFormTpl from "./form.component.vue";

interface Validities {
	[key: string]: boolean;
}

@Component({
	components: { ValidationObserver }
})
export class RctForm extends Mixins(RctFormTpl) {
	@Prop({ default: "Save" })
	public saveLabel: string;

	@Prop({ default: "Cancel" })
	public cancelLabel: string;

	@Prop({ default: true })
	public showNav: boolean;

	@Emit()
	public save(): boolean {
		return true;
	}

	@Emit()
	public cancel(): boolean {
		return false;
	}
}
