import Vue, { Component } from "vue";
import VeeValidate from "vee-validate";
import CKEditor from "@ckeditor/ckeditor5-vue";
import kebabCase from "lodash/kebabCase";
import * as Controls from "./controls";

import { RctForm } from "./form/form.component";

import "./forms.scss";

interface LoopComponent {
	component: Component;
	name: string;
}

Vue.use(CKEditor);
Vue.use(VeeValidate);

export class RctFormsPlugin {
	public static install(): void {
		Object.keys(Controls)
			.map(
				(key): LoopComponent => {
					return { component: Controls[key], name: kebabCase(key) };
				}
			)
			.forEach(({ component, name }): void => {
				// eslint-disable-next-line @typescript-eslint/no-explicit-any
				Vue.component(name, component as any);
			});
		Vue.component("rct-form", RctForm);
	}
}
