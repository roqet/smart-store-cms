import axios from "axios";
import { Cloudinary } from "cloudinary-core";

const uploader = axios.create({
	baseURL: process.env.CLOUDINARY_UPLOAD_URL,
	headers: { "Content-Type": "multipart/form-data" }
});

export class ImageService {
	private static cloudinary = Cloudinary.new({
		// eslint-disable-next-line @typescript-eslint/camelcase
		cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
		secure: true
	});

	public constructor() {}

	private static data(file): FormData {
		let form = new FormData();
		form.append("file", file);
		form.append("upload_preset", process.env.CLOUDINARY_UPLOAD_PRESET);
		return form;
	}

	public static upload(file: File): Promise<Cloud.Image> {
		return uploader
			.post("", this.data(file))
			.then(({ data }): Cloud.Image => data);
	}

	// eslint-disable-next-line @typescript-eslint/camelcase
	public static url({ public_id }, dimension: number): string {
		return this.cloudinary.url(public_id, {
			width: dimension,
			height: dimension,
			crop: "fill"
		});
	}
}
