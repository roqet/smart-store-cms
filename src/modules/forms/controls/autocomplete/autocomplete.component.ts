import { Component, Mixins, Prop, Emit, Watch } from "vue-property-decorator";
import { RctFormControl, Model } from "../base";
import RctFormAutocompleteTpl from "./autocomplete.component.vue";

interface Option {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	[key: string]: any;
	formatted: string;
}

@Component
export class RctFormAutocomplete extends Mixins(
	RctFormControl,
	RctFormAutocompleteTpl
) {
	@Prop({ default: (): Option[] => [] }) private options: Option[];
	@Prop({ default: "id" }) private optValue: string;
	@Prop({ default: "title" }) private optLabel: string;

	public mdOptions: string[] = [];

	@Emit("change")
	public changed(value): Model {
		return ((option): Model => {
			if (option) return option[this.optValue];
			return "";
		})(
			this.options.find((option): boolean => {
				return option[this.optLabel] == value;
			})
		);
	}

	@Watch("options")
	public setvalues(): void {
		this.mdSearch();
		this.value = this.formatter();
	}

	public mdSearch(term: string = ""): void {
		this.mdOptions = this.options
			.map(
				(option): Option => ({
					...option,
					formatted: option[this.optLabel].toLowerCase()
				})
			)
			.filter((option): boolean => {
				return term
					? option.formatted.includes(term.toLowerCase())
					: true;
			})
			.map((option): string => {
				return option[this.optLabel];
			});
	}

	public formatter(): Model {
		return ((option): Model => {
			return option ? option[this.optLabel] : "";
		})(
			this.options.find((option): boolean => {
				return option[this.optValue] == this.model;
			})
		);
	}
}
