import { Component, Mixins, Prop } from "vue-property-decorator";
import Editor from "@ckeditor/ckeditor5-build-classic";
import RctFormCkeditorTpl from "./ckeditor.component.vue";
import { RctFormControl } from "./../base";
import "./ckeditor.component.scss";

@Component
export class RctFormCkeditor extends Mixins(
	RctFormControl,
	RctFormCkeditorTpl
) {
	@Prop({ type: Object, default: (): object => ({}) }) public config: object;

	public editor = Editor;
	public value: string = "";

	public formatter(): string {
		return this.model || "";
	}
}
