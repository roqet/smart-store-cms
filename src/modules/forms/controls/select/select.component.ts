import { Component, Mixins, Prop } from "vue-property-decorator";
import { RctFormControl } from "./../base";
import RctFormControlTpl from "./select.component.vue";

interface Option {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	[key: string]: any;
}

@Component
export class RctFormSelect extends Mixins(RctFormControl, RctFormControlTpl) {
	@Prop({ type: Array, default: (): [] => [] }) public options: Option[];
	@Prop({ default: "id" }) public optValue: string;
	@Prop({ default: "title" }) public optLabel: string;
}
