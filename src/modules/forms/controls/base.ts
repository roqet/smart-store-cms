import {
	Vue,
	Prop,
	Emit,
	Model,
	Component,
	Watch
} from "vue-property-decorator";
import { ValidationProvider } from "vee-validate";
import CyrillicToTranslit from "cyrillic-to-translit-js";
import kebabCase from "lodash/kebabCase";
import { ValidationRules } from "./../validation/validation.rules";

interface IconProp {
	custom?: string;
	md?: string;
}

const cyrillicToTranslit = new CyrillicToTranslit();

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Model = any;

@Component({
	components: { ValidationProvider }
})
export class RctFormControl extends Vue {
	@Prop({ type: Object, default: (): IconProp => ({}) })
	public readonly icon: IconProp;

	@Prop(String)
	public readonly label!: string;

	@Prop(String)
	public readonly description: string;

	@Prop(Boolean)
	public readonly disabled: boolean;

	@Prop({ default: (): object => ({}) })
	public readonly validation: ValidationRules;

	@Model("change")
	public readonly model!: Model;

	public value: Model = null;
	public type: string = "control";

	private pristine: boolean = true;

	@Emit("change")
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	public changed(value): Model {
		return this.value;
	}

	@Watch("model")
	public setvalue(): void {
		this.value = this.formatter();
	}

	public get hasIcon(): boolean {
		return !!this.icon.custom || !!this.icon.md;
	}

	public get customIcon(): string {
		return this.icon && this.icon.custom ? this.icon.custom : "";
	}

	public get mdIcon(): string {
		return this.icon && this.icon.md ? this.icon.md : "";
	}

	public get id(): string {
		return ((translit): string => {
			return `rct-form-${this.type}-${kebabCase(translit)}`;
		})(cyrillicToTranslit.transform(this.label));
	}

	public get isRequired(): boolean {
		return this.validation.required;
	}

	public formatter(): Model {
		return this.model;
	}

	public init(): void {
		this.value = this.formatter();
	}

	public mounted(): void {
		this.init();
	}

	public setPristine(value: boolean): void {
		this.pristine = value;
	}
}
