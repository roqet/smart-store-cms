import { Component, Mixins, Model } from "vue-property-decorator";
import RctFormVirtualTpl from "./virtual.component.vue";

@Component
export class RctFormVirtual extends Mixins(RctFormVirtualTpl) {
	@Model("change")
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	public readonly model!: any;
}
