import { Component, Mixins } from "vue-property-decorator";
import { RctFormControl } from "./../base";
import RctFormCheckboxTpl from "./checkbox.component.vue";

@Component
export class RctFormCheckbox extends Mixins(
	RctFormControl,
	RctFormCheckboxTpl
) {
	public type: string = "checkbox";
}
