import { Component, Prop, Watch, Mixins, Emit } from "vue-property-decorator";
// import Debounce from "debounce-decorator"
import debounce from "lodash/debounce";
import { RctFormControl, Model } from "../base";
import RctFormAutocompleteAsyncTpl from "./autocomplete.async.component.vue";
import { Autocomplete } from "../../decorators/autocomplete/autocomplete.component";

@Component({
	components: { autocomplete: Autocomplete }
})
export class RctFormAutocompleteAsync extends Mixins(
	RctFormControl,
	RctFormAutocompleteAsyncTpl
) {
	@Prop() private options: (q: string, init?: boolean) => Promise<object[]>;
	@Prop({ default: "id" }) private optValue: string;
	@Prop({ default: "title" }) public optLabel: string;
	@Prop({ default: "model" }) public optModel: string;
	@Prop({ default: "string" }) private format: "object" | "string";

	public type: string = "autocomplete";
	public loading: boolean = false;
	public lookup: object[] = [];

	@Emit("change")
	public changed(): Model {
		if (this.format === "object") {
			return this.lookup.find((option): boolean => {
				return option[this.optValue] == this.value;
			})[this.optModel];
		} else {
			return this.value;
		}
	}

	@Watch("model")
	public setvalue(): void {
		if (!this.value) {
			this.loading = true;
			this.defopts().then((lookup): void => {
				this.lookup = lookup;
				this.loading = false;
			});
		}
		this.value = this.formatter();
	}

	public search(): void {}

	public created(): void {
		this.search = debounce((query): void => {
			this.loading = true;
			this.options(query).then((lookup): void => {
				this.lookup = lookup;
				this.loading = false;
			});
		}, 300);
	}

	public mounted(): void {
		this.value = this.formatter();
		this.loading = true;
		this.defopts().then((lookup): void => {
			this.lookup = lookup;
			this.loading = false;
		});
	}

	private defopts(): Promise<object[]> {
		if (this.model) {
			return this.options(this.model, true);
		} else {
			return Promise.resolve([]);
		}
	}
}
