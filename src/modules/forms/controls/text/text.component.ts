import { Component, Mixins } from "vue-property-decorator";
import { RctFormControl } from "./../base";
import RctFormTextTpl from "./text.component.vue";

@Component
export class RctFormText extends Mixins(RctFormControl, RctFormTextTpl) {
	public type: string = "text";
}
