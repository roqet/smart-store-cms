import { Component, Mixins } from "vue-property-decorator";
import { IModalConfig } from "./moda.types";
import { ModalService } from "./modal.service";

import { ShopModal } from "@/shop/modals/shop/shop.component";

@Component
export class UseModal extends Mixins() {
	private findModal(modal: string): IModalConfig {
		return ModalService.modals.find(({ name }): any => {
			return modal == name;
		});
	}

	public $modal(name: string, params: object = {}): Promise<any> {
		return new Promise((resolve) => {
			(({ component, size = "md", name }: IModalConfig) => {
				this.$dialog.modal(ShopModal, {
					title: false,
					params: params
				});
			})(this.findModal(name));
		});
	}
}
