import Vue, { Component } from "vue";
import VDialogs from "v-dialogs";

import { IModalConfig } from "./moda.types";
import { ModalService } from "./modal.service";
import { ModalSave } from "./modal-save/modal.save.component";

//import { ModalDirective } from "./modal.directive";

Vue.use(VDialogs, {
	instanceName: "$dialog"
});

declare module "vue/types/vue" {
	interface Vue {
		$dialog: {
			modal: (component: Component, params?: object) => void;
		};
	}
}

export class RctModalPlugin {
	public static install(Vue, modals: IModalConfig[]): void {
		ModalService.modals = modals;
		Vue.component("modal-save-tpl", ModalSave);
	}
}

export * from "./moda.types";
export * from "./use.modal.mixin";
