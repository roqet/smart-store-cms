import { Component } from "vue";

export type ModalSize = "sm" | "md" | "lg";

export interface ModalDimentions {
	width: number;
	height: number;
}

export interface IModalConfig {
	name: string;
	component: Component;
	size?: ModalSize;
}
