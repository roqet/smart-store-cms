import { Component, Mixins } from "vue-property-decorator";
import ModalSaveTpl from "./modal.save.component.vue";

@Component
export class ModalSave extends Mixins(ModalSaveTpl) {}
