import Vue, { Component } from "vue";
import { IModalConfig } from "./moda.types";

export class ModalService {
	private static _modals: IModalConfig[] = [];

	public static set modals(modals: IModalConfig[]) {
		this._modals = modals;
	}

	public static get modals(): IModalConfig[] {
		return this._modals;
	}

	// public static config(modals: IModalConfig[] = []):void {
	//     this.modals = [...this.modals, ...modals];
	// };

	public static open(name: string, params: object = {}): void {
		(<any>Vue).dialog.alert("", {
			view: name
		});
	}
}
