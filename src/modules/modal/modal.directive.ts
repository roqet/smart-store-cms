import { DirectiveOptions } from "vue";
import { ModalService } from "./modal.service";

export const ModalDirective:DirectiveOptions = {

    bind: (el, { value }) => {
        el.addEventListener("click", () => {
            ModalService.open(value)
        })
    }

};