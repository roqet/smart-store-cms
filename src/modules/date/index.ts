import moment from "moment";
import { DateService } from "./date.service";

class I18N {
	public date: DateService;
	public moment: moment.Moment;

	public constructor() {
		this.date = new DateService();
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		this.moment = moment as any;
	}
}

export const i18n = new I18N();

export class RctI18nPlugin {
	public static install(): void {}
}
