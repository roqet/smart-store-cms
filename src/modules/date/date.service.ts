import moment from "moment";

export class DateService {
	public constructor() {
		moment.locale(this.language());
	}

	private language(): string {
		if (window.navigator.languages) {
			return window.navigator.languages[0];
		} else {
			return (
				window.navigator["userLanguage"] || window.navigator.language
			);
		}
	}

	public longDateFormat(format: moment.LongDateFormatKey): string {
		return moment.localeData().longDateFormat(format);
	}

	public firtsDayOfWeek(): number {
		return moment.localeData().firstDayOfWeek();
	}

	public hour(date: Date | number): number {
		return moment(date).hour();
	}
}
