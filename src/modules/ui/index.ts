import Vue from "vue";
import VueMaterial from "vue-material";

import { RctLoader } from "./loader/loader.component";
import { RctPage } from "./page/page.component";
import { RctImg } from "./img/img.component";
import { RctNoResults } from "./no-results/no.results.component";
import { RctLoaderOverlay } from "./loader-overlay/loader.overlay.component";
import { RctLayoutItem } from "./layout-item/layout.item.drective";

import "./styles/styles.scss";

Vue.use(VueMaterial);

export class RctUiPlugin {
	public static install(): void {
		Vue.component("rct-loader", RctLoader);
		Vue.component("rct-page", RctPage);
		Vue.component("rct-img", RctImg);
		Vue.component("rct-no-results", RctNoResults);
		Vue.component("rct-loader-overlay", RctLoaderOverlay);
		Vue.directive("rct-layout-item", RctLayoutItem);
	}
}
