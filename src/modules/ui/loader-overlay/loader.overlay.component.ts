import { Component, Mixins, Prop } from "vue-property-decorator";
import RctLoaderOverlayTpl from "./loader.overlay.component.vue";

@Component
export class RctLoaderOverlay extends Mixins(RctLoaderOverlayTpl) {
	@Prop({ default: false }) private progress: boolean;
}
