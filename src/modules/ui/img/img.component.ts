import { Component, Mixins, Prop } from "vue-property-decorator";
import { Cloudinary } from "cloudinary-core";
import RctImgTpl from "./img.component.vue";

const cloudinary = Cloudinary.new({
	// eslint-disable-next-line @typescript-eslint/camelcase
	cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
	secure: true
});

@Component
export class RctImg extends Mixins(RctImgTpl) {
	@Prop({ default: (): object => ({}) }) private src: Cloud.Image;
	@Prop({ default: 100 }) private width: number;
	@Prop({ default: 0 }) private height: number;
	@Prop({ default: "thumb" }) private crop: string;

	public get source(): string {
		return cloudinary.url(this.src.public_id, {
			width: this.width * window.devicePixelRatio,
			height: (this.height || this.width) * window.devicePixelRatio,
			crop: this.crop
		});
	}
}
