import Vue from "vue";
import VueI18n, { LocaleMessages } from "vue-i18n";
import moment from "moment";
import { DateFilter } from "./date.filter";
import "moment/locale/uk";

Vue.use(VueI18n);

declare module "vue/types/vue" {
	interface Vue {
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		$moment: any;
	}
}

interface I18N {
	locale: string;
	messages: {
		ua: LocaleMessages;
	};
}

export class RctI18nPlugin {
	public static install(Vue, { locale = "uk" } = {}): void {
		moment.locale(locale);
		Vue.filter("date", DateFilter);
		Vue.prototype.$moment = moment;
	}
}

export const i18n = ({ translations }): I18N => {
	return {
		locale: "ua",
		messages: {
			ua: translations
		}
	};
};

export * from "./date.service";
