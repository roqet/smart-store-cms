import moment from "moment";

export const DateFilter = (
	value: Date | number | string,
	format: string = "LT"
): string => {
	return moment(value).format(format);
};
