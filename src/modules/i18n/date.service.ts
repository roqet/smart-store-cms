import moment from "moment";
import capitalize from "lodash/capitalize";

export interface IWeekday {
	id: number;
	title: string;
}

export class DateService {
	public static weekdays(): IWeekday[] {
		return [
			{
				id: 0,
				title: capitalize(moment.weekdays(1))
			},
			{
				id: 1,
				title: capitalize(moment.weekdays(2))
			},
			{
				id: 2,
				title: capitalize(moment.weekdays(3))
			},
			{
				id: 3,
				title: capitalize(moment.weekdays(4))
			},
			{
				id: 4,
				title: capitalize(moment.weekdays(5))
			},
			{
				id: 5,
				title: capitalize(moment.weekdays(6))
			},
			{
				id: 6,
				title: capitalize(moment.weekdays(0))
			}
		];
	}
}
