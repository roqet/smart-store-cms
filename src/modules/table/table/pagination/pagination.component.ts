import { Component, Mixins, Prop, Emit, Watch } from "vue-property-decorator";
import RctTablePaginationTpl from "./pagination.component.vue";

@Component
export class RctTablePagination extends Mixins(RctTablePaginationTpl) {
	@Prop({ default: 0, type: Number }) private total: number;
	@Prop({ default: 25 }) private count: number;
	@Prop({ default: 1 }) private page: number;
	@Prop({ default: (): number[] => [25, 50, 100] })
	public pageOptions: number[];
	@Prop({ default: "Rows per page:" }) private label: string;
	@Prop({ default: "of" }) public separator: string;
	@Prop({ default: false }) public inprogress: boolean;

	public currentPageSize: number = 0;

	@Emit("update:page")
	public goToNext(): number {
		return this.page + 1;
	}

	@Emit("update:page")
	public goToPrev(): number {
		return this.page - 1;
	}

	@Emit("update:count")
	public setPageSize(): number {
		return this.currentPageSize;
	}

	@Watch("mdPageSize", { immediate: true })
	public onMdPageSize(): void {
		this.currentPageSize = this.count;
	}

	// // @Watch("mdPage", {immediate: true})
	// // public onMdPage(value):void {
	// //     this.currentPageSize = this.mdPageSize
	// // };

	public get lastPage(): number {
		if (this.total == 0) {
			return 1;
		}
		if (this.total % this.count == 0) {
			this.total % this.count;
		}
		return Math.trunc(this.total / this.count) + 1;
	}

	public get currentItemCount(): number {
		return (this.page - 1) * this.count + 1;
	}

	public get currentPageCount(): number {
		return ((count): number => {
			if (count > this.total) return this.total;
			else return count;
		})(this.page * this.count);
	}

	public get isFirstPage(): boolean {
		return this.page == 1;
	}

	public get isLastPage(): boolean {
		return this.page == this.lastPage;
	}
}
