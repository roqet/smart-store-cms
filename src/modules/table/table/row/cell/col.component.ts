import { Component, Mixins, Prop } from "vue-property-decorator";
import RctTableColTpl from "./col.component.vue";

@Component
export class RctTableCell extends Mixins(RctTableColTpl) {
	@Prop({ default: "" }) public heading: string;
	@Prop({ default: "" }) public sort: string;
}
