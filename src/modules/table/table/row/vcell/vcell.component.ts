import { Component } from "vue";

export const RctTableVcell: Component = {
	functional: true,
	props: {
		row: { type: Object },
		index: { type: Number },
		col: { type: Object }
	},
	inject: ["rcolumn"],
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	render: (createElement, context): any => {
		return context.injections.rcolumn()({
			row: context.props.row
		})[context.props.index];
	}
};
