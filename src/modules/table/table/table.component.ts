import { Component, Mixins, Prop, Provide } from "vue-property-decorator";
import RctTableTpl from "./table.component.vue";
import { RctTableVcell } from "./row/vcell/vcell.component";
import { RctTablePagination } from "./pagination/pagination.component";
import { RctTableCell } from "./row/cell/col.component";

import "./table.component.scss";

interface IDataResponse {
	records: object[];
	total: number;
}

interface IDataParams {
	q?: string;
	page: number;
	count: number;
}

interface ISearch {
	[key: string]: string;
}

interface Table {
	records: object[];
	query: string;
}

interface Column {
	index: number;
	heading: string;
	sort: string;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	col: any;
}

@Component({
	components: {
		"rct-table-vcell": RctTableVcell,
		"rct-table-pagination": RctTablePagination
	}
})
export class RctTable extends Mixins(RctTableTpl) {
	@Prop() private data: (params: IDataParams) => Promise<IDataResponse>;
	// @Prop({ default: (): object => ({}) }) public sorting: any;
	@Prop({ default: (): object => ({}) }) private search: ISearch;

	public inprogress: boolean = false;

	public total: number = 0;
	public page: number = 1;
	public count: number = 25;
	public records: object[] = [];

	public table: Table = {
		records: [],
		query: ""
	};

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	@Provide() public rcolumn = (): any => this.$scopedSlots.row;

	// public search():void {
	//     console.log(this.table.query)
	// };

	public get hasSearch(): boolean {
		return !!Object.keys(this.search).length;
	}

	public get noResultsLabel(): string {
		return "No results found";
	}

	public get noResultsDescription(): string {
		return "No results found. Try a different search term, apply other filters or create a new item.";
	}

	public onPageChanged(page: number): void {
		this.page = page;
		this.load();
	}

	public onCountChanged(count: number): void {
		this.page = 1;
		this.count = count;
		this.load();
	}

	public load(): void {
		this.inprogress = true;
		this.data({
			// q: this.table.query,
			page: this.page - 1,
			count: this.count
		})
			.then(({ records, total }): void => {
				this.total = total;
				this.records = records;
				this.inprogress = false;
			})
			.catch(({ errors }): void => {
				this.inprogress = false;
				errors.forEach((error): void => this.$toast.error(error));
			});
	}

	public get columns(): Column[] {
		return this.$scopedSlots
			.row({ row: {} })
			.map(
				(col, index): Column => {
					return (({ heading, sort }: RctTableCell): Column => ({
						index,
						heading,
						sort,
						col
					}))((col.componentOptions || { propsData: {} })
						.propsData as RctTableCell);
				}
			)
			.filter(({ heading }): boolean => !!heading);
	}

	public mounted(): void {
		this.load();
	}

	public created(): void {}
}
