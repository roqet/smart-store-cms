import Vue from "vue";
import { RctToastPlugin } from "@modules/toast";

import { RctTable } from "./table/table.component";
import { RctTableCell } from "./table/row/cell/col.component";

Vue.use(RctToastPlugin);

export class RctTablePlugin {
	public static install(): void {
		Vue.component("rct-table", RctTable);
		Vue.component("rct-table-cell", RctTableCell);
	}
}

export { RctTable };
