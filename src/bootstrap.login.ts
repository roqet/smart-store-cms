import Vue, { VNode } from "vue";

import { translations } from "@i18n/translations";
import { SmartStoreThemePlugin } from "@theme/smartstore";
import { RctI18nPlugin, i18n } from "@modules/i18n";
import { RctFormsPlugin } from "@modules/forms";
import { Login, store } from "@login";

Vue.use(RctI18nPlugin);
Vue.use(SmartStoreThemePlugin);
Vue.use(RctFormsPlugin);

new Vue({
	i18n: i18n({ translations }),
	render: (el): VNode => el(Login),
	store
}).$mount("#login");
