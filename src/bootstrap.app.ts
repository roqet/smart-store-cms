import Vue, { VNode } from "vue";

import { translations } from "@i18n/translations";
import { SmartStoreThemePlugin } from "@theme/smartstore";
import { RctI18nPlugin, i18n } from "@modules/i18n";
// import { RctModalPlugin } from "@modules/modal";
import { RctToastPlugin } from "@modules/toast";
import { RctFormsPlugin } from "@modules/forms";
import { RctUiPlugin } from "@modules/ui";
import { RctTablePlugin } from "@modules/table";
import { RctShellPlugin, Shell } from "@/shell";

import { store, router } from "@configs/all";

Vue.use(RctI18nPlugin);
Vue.use(SmartStoreThemePlugin);
Vue.use(RctToastPlugin);
Vue.use(RctFormsPlugin);
Vue.use(RctUiPlugin);
Vue.use(RctTablePlugin);

Vue.use(RctShellPlugin, {
	navigation: [
		{
			anchor: "pages.home",
			icon: "home",
			state: { name: "home" }
		},
		{
			anchor: "pages.shopsSlashCitiesSlashPrices",
			icon: "location",
			state: { name: "shops" }
		},
		{
			anchor: "pages.newsSlashPromotions",
			icon: "gift",
			state: { name: "news" }
		},
		{
			anchor: "pages.contacts",
			icon: "contact",
			state: { name: "contacts" }
		}
	],
	submenus: {
		shops: {
			show: ["shops", "cities", "prices"],
			submenu: [
				{
					anchor: "pages.shops",
					state: { name: "shops" }
				},
				{
					anchor: "pages.cities",
					state: { name: "cities" }
				},
				{
					anchor: "pages.prices",
					state: { name: "prices" }
				}
			]
		}
	}
});

new Vue({
	i18n: i18n({ translations }),
	render: (el): VNode => el(Shell),
	store,
	router
}).$mount("#app");
