import Vue from "vue";
import Vuex, { Store } from "vuex";

import { ShopStore, ShopsStore } from "@/shop/store";
import { NewsStore } from "@/news/store";
import { ContactsStore } from "@/contacts/store";
import { CityStore } from "@/city/store";
import { GlobalStore } from "@/common/store";
import { AboutStore } from "@/home/store";
import { PricesStore } from "@/price/store";
import { ShellStore } from "@/shell/store";

Vue.use(Vuex);

export const store = new Store({
	modules: {
		global: GlobalStore,
		shop: ShopStore,
		shops: ShopsStore,
		news: NewsStore,
		contacts: ContactsStore,
		city: CityStore,
		about: AboutStore,
		prices: PricesStore,
		shell: ShellStore
	}
});
