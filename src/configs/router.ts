import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

import { HomeRoutes } from "@/home";
import { ShopRoutes } from "@/shop";
import { NewsRoutes } from "@/news";
import { ContactsRoutes } from "@/contacts";
import { CityRoutes } from "@/city";
import { PriceRoutes } from "@/price";

Vue.use(VueRouter);

const GlobalRoutes: RouteConfig[] = [
	{
		path: "/",
		redirect: "home"
	}
];

export const router = new VueRouter({
	routes: [
		...GlobalRoutes,
		...HomeRoutes,
		...ShopRoutes,
		...NewsRoutes,
		...ContactsRoutes,
		...CityRoutes,
		...PriceRoutes
	]
});
