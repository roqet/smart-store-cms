import { IModalConfig } from "@modules/modal";

import { ShopModal } from "./../modals/shop/shop.component";

export const ModalsConfig: IModalConfig[] = [
	{
		name: "shop",
		component: ShopModal
	}
];
