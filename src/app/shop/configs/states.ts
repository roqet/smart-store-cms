import { RouteConfig } from "vue-router";

import { ShopsPage } from "./../states/shops/shops.component";
import { ManageShopPage } from "./../states/manage/manage.component";

export const ShopRoutes: RouteConfig[] = [
	{
		path: "/shops",
		name: "shops",
		component: ShopsPage,
		meta: { root: "shops" }
	},
	{
		path: "/shops/create",
		name: "createShop",
		component: ManageShopPage,
		meta: { root: "shops" }
	},
	{
		path: "/shops/:id/edit",
		name: "editShop",
		component: ManageShopPage,
		meta: { root: "shops" }
	}
];
