export interface Meta {
	city: string;
	address: string;
}

export interface ShopState {
	loading: boolean;
	shop: Partial<Store.Shop.Shop>;
	cities: Store.City.City[];
	meta: Partial<Meta>;
}

export const ShopInitialState = (): ShopState => ({
	loading: false,
	shop: {
		address: { id: undefined, name: undefined, cityId: undefined },
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		workingSchedule: new Array(7).fill(0).map(
			(item, index): Store.Shop.WorkingSchedule => ({
				start: "09:00",
				end: "18:00",
				isClosed: false,
				dayNum: index
			})
		)
	},
	cities: [],
	meta: {}
});

// export const ShopInitialState = (): ShopState => ({
// 	loading: false,
// 	shop: {
// 		dayOfDelivery: 1,
// 		deliveryLux: 2,
// 		address: {
// 			id:
// 				"EihHcm9jaG93c2thLCDQktCw0YDRiNCw0LLQsCwg0J_QvtC70YzRidCwIi4qLAoUChIJc-V0k5XNHkcRt_rgZ3gBrcwSFAoSCQGfhppmzB5HEfzT6ogqvvBy",
// 			name: "Grochowska",
// 			cityId: "1"
// 		},
// 		workingSchedule: new Array(7).fill(0).map(
// 			(item, index): Store.Shop.WorkingSchedule => ({
// 				start: "08:30",
// 				end: "19:00",
// 				isClosed: false,
// 				dayNum: index + 1
// 			})
// 		)
// 	},
// 	cities: [],
// 	meta: { city: 1 }
// });
