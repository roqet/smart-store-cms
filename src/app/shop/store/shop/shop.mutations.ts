import { ShopState, ShopInitialState } from "./shop.state";

enum Types {
	LOADING = "[SHOP] LOADING",
	SET_CITIES = "[SHOP] SET_CITIES",
	RESET_SHOP = "[SHOP] RESET_SHOP",
	SET_SHOP = "[SHOP] SET_SHOP"
}

interface ShopMutations {
	[key: string]: (state: ShopState, payload: ShopActions.All) => void;
}

export const ShopMutations = {
	[Types.LOADING]: (state: ShopState, { loading }: ShopActions.All): void => {
		state.loading = loading;
	},
	[Types.SET_CITIES]: (
		state: ShopState,
		{ cities }: ShopActions.All
	): void => {
		state.cities = cities;
	},
	[Types.RESET_SHOP]: (state: ShopState): void => {
		((initial: ShopState): void => {
			state.shop = initial.shop;
			state.meta = initial.meta;
		})(ShopInitialState());
	},
	[Types.SET_SHOP]: (state: ShopState, { shop }: ShopActions.All): void => {
		state.shop = shop;
		state.meta = { city: shop.address.cityId, address: shop.address.name };
	}
};

export namespace ShopActions {
	export class Loading {
		public readonly type: Types = Types.LOADING;
		public constructor(public loading: boolean) {}
	}

	export class SetCities {
		public readonly type: Types = Types.SET_CITIES;
		public constructor(public cities: Store.City.City[]) {}
	}

	export class ResetShop {
		public readonly type: Types = Types.RESET_SHOP;
	}

	export class SetShop {
		public readonly type: Types = Types.SET_SHOP;
		public constructor(public shop: Store.Shop.Shop) {}
	}

	export type All = Loading & SetCities & ResetShop & SetShop;
}
