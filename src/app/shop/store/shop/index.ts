import { Module } from "vuex";

import { ShopInitialState, ShopState } from "./shop.state";
import { ShopMutations } from "./shop.mutations";
import { ShopOperations } from "./shop.operations";

export const ShopStore: Module<ShopState, {}> = {
	state: ShopInitialState,
	mutations: ShopMutations,
	actions: ShopOperations,
	namespaced: true
};
