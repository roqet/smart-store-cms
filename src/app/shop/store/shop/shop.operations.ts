import { ActionContext } from "vuex";
import { SmartStoreApi } from "@api/smartstore";
import { GlobalActions } from "@/common/store";
import { ShopState } from "./shop.state";
import { ShopActions } from "./shop.mutations";

export const ShopOperations = {
	reset: ({ commit }: ActionContext<ShopState, {}>): void => {
		commit(new ShopActions.ResetShop());
	},

	init: ({ commit }: ActionContext<ShopState, {}>, id: number): void => {
		Promise.resolve(commit(new ShopActions.Loading(true)))
			.then(
				(): Promise<
					[pagination.Response<Store.City.City>, Store.Shop.Shop]
				> => {
					return Promise.all([
						SmartStoreApi.city.query({ page: 0, count: 9999 }),
						id ? SmartStoreApi.shop.get(id) : Promise.resolve(null)
					]);
				}
			)
			.then(([cities, shop]): void => {
				if (id) commit(new ShopActions.SetShop(shop));
				commit(new ShopActions.SetCities(cities.records));
				commit(new ShopActions.Loading(false));
			})
			.catch((error): void => {
				const { errors, type, options } = new GlobalActions.Errors(
					error.errors
				);
				commit(type, errors, options);
				commit(new ShopActions.Loading(false));
			});
	},

	save: (
		{ commit }: ActionContext<ShopState, {}>,
		shop: Store.Shop.Shop
	): void => {
		Promise.resolve(commit(new ShopActions.Loading(true)))
			.then(
				(): Promise<Store.Shop.Shop> => {
					if (shop.id) return SmartStoreApi.shop.update(shop);
					else return SmartStoreApi.shop.create(shop);
				}
			)
			.then(
				({ address }): GlobalActions.Messages => {
					console.log(address);
					return new GlobalActions.Messages([
						{
							i18n: `messages.success.shop${
								shop.id ? "Updated" : "Created"
							}`,
							params: { address: address.name }
						}
					]);
				}
			)
			.then(({ messages, type, options }): void => {
				commit(new ShopActions.Loading(false));
				commit(type, messages, options);
				if (!shop.id) commit(new ShopActions.ResetShop());
			})
			.catch((error): void => {
				const { errors, type, options } = new GlobalActions.Errors(
					error.errors
				);
				commit(type, errors, options);
				commit(new ShopActions.Loading(false));
			});
	}
};
