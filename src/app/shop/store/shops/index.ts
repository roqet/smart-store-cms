import { Module } from "vuex";

import { ShopsInitialState, ShopsState } from "./shops.state";
import { ShopsMutations } from "./shops.mutations";
import { ShopsOperations } from "./shops.operations";

export const ShopsStore: Module<ShopsState, {}> = {
	state: ShopsInitialState,
	mutations: ShopsMutations,
	actions: ShopsOperations,
	namespaced: true
};
