export interface Meta {
	city: string;
}

export interface ShopsState {
	loading: boolean;
	cities: Store.City.City[];
	meta: Partial<Meta>;
}

export const ShopsInitialState = (): ShopsState => ({
	loading: false,
	cities: [],
	meta: {}
});
