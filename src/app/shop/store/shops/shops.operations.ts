import { ActionContext } from "vuex";
import { SmartStoreApi } from "@api/smartstore";
import { GlobalActions } from "@/common/store";
import { ShopsState } from "./shops.state";
import { ShopsActions } from "./shops.mutations";

export const ShopsOperations = {
	init: ({ commit }: ActionContext<ShopsState, {}>): void => {
		Promise.resolve(commit(new ShopsActions.Loading(true)))
			.then(
				(): Promise<pagination.Response<Store.City.City>> => {
					return SmartStoreApi.city.query({ page: 0, count: 9999 });
				}
			)
			.then(({ records }): void => {
				commit(new ShopsActions.SetCities(records));
				commit(new ShopsActions.SetMeta(records[0]));
				commit(new ShopsActions.Loading(false));
			})
			.catch((error): void => {
				const { errors, type, options } = new GlobalActions.Errors(
					error.errors
				);
				commit(type, errors, options);
				commit(new ShopsActions.Loading(false));
			});
	}
};
