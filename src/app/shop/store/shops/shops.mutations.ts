import { ShopsState } from "./shops.state";

enum Types {
	LOADING = "[SHOP] LOADING",
	SET_CITIES = "[SHOP] SET_CITIES",
	SET_META = "[SHOP] SET_META"
}

interface ShopMutations {
	[key: string]: (state: ShopsState, payload: ShopsActions.All) => void;
}

export const ShopsMutations: ShopMutations = {
	[Types.LOADING]: (
		state: ShopsState,
		{ loading }: ShopsActions.All
	): void => {
		state.loading = loading;
	},
	[Types.SET_CITIES]: (
		state: ShopsState,
		{ cities }: ShopsActions.All
	): void => {
		state.cities = cities;
	},
	[Types.SET_META]: (state: ShopsState, { city }: ShopsActions.All): void => {
		state.meta = { city: city && city.id.toString() };
	}
};

export namespace ShopsActions {
	export class Loading {
		public readonly type: Types = Types.LOADING;
		public constructor(public loading: boolean) {}
	}

	export class SetCities {
		public readonly type: Types = Types.SET_CITIES;
		public constructor(public cities: Store.City.City[]) {}
	}

	export class SetMeta {
		public readonly type: Types = Types.SET_META;
		public constructor(public city: Store.City.City) {}
	}

	export type All = Loading & SetCities & SetMeta;
}
