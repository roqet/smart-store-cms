import { Component, Mixins } from "vue-property-decorator";
import ShopModalTpl from "./shop.component.vue";

@Component
export class ShopModal extends Mixins(ShopModalTpl) {
	public mounted(): void {
		console.log(this.$t("navigation.shops"));
	}
}
