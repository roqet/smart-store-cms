import toNumber from "lodash/toNumber";
import merge from "lodash/merge";
import { Component, Mixins } from "vue-property-decorator";
import { LocaleMessage } from "vue-i18n";
import { namespace } from "vuex-class";
import { SmartStoreApi } from "@api/smartstore";
import { DateService, IWeekday } from "@modules/i18n";
import { UseGlobalStore } from "@/common/mixins/use.global.store";
import { Meta } from "../../store/shop/shop.state";
import ManageShopPageTpl from "./manage.component.vue";

const Store = namespace("shop");

@Component
export class ManageShopPage extends Mixins(UseGlobalStore, ManageShopPageTpl) {
	@Store.State("loading") public loading: boolean;
	@Store.State("shop") public shop: Store.Shop.Shop;
	@Store.State("cities") public cities: Store.City.City[];
	@Store.State("meta") public meta: Meta;
	@Store.Action("reset") private reset: () => void;
	@Store.Action("init") private init: (id: number) => void;
	@Store.Action("save") private saveShop: (shop: Store.Shop.Shop) => void;

	public weekdays: IWeekday[] = DateService.weekdays();
	public addressName: string;

	private addressParams(input: string): store.location.AddressParams {
		return {
			input,
			geometry: this.cities.find(({ id }): boolean => {
				return toNumber(id) === toNumber(this.meta.city);
			}).geometry
		};
	}

	public address(q, init: boolean): Promise<object[]> {
		if (init) {
			return Promise.resolve([
				{
					title: q.name,
					id: q.id,
					model: q
				}
			]);
		}
		return SmartStoreApi.city
			.address(this.addressParams(q))
			.then((addresses: store.location.Address[]): object[] => {
				return addresses.map((item): object => {
					return {
						title: item.name,
						id: item.id,
						model: { ...item, cityId: this.meta.city }
					};
				});
			});
	}

	public get title(): LocaleMessage {
		if (this.$route.params.id) {
			return this.$t("pages.editShop");
		} else {
			return this.$t("pages.createShop");
		}
	}

	public cityChanged(value): void {
		if (!value) {
			this.shop.address = undefined;
			this.meta.address = "";
		}
	}

	public save(): void {
		this.saveShop(
			merge({}, this.shop, {
				address: { name: this.meta.address }
			})
		);
	}

	public cancel(): void {
		this.$router.push({ name: "shops" });
	}

	public mounted(): void {
		this.init(parseInt(this.$route.params.id));
	}

	public destroyed(): void {
		this.reset();
	}
}
