import { Component, Mixins, Watch } from "vue-property-decorator";
import { namespace } from "vuex-class";
import { SmartStoreApi } from "@api/smartstore";
import { RctTable } from "@modules/table";
import { DateService, IWeekday } from "@modules/i18n";
import { UseGlobalStore } from "@/common/mixins";
import EventsPageTpl from "./shops.component.vue";
import { Meta } from "./../../store/shops/shops.state";

const Store = namespace("shops");

@Component
export class ShopsPage extends Mixins(EventsPageTpl, UseGlobalStore) {
	@Store.State("loading")
	public loading: boolean;

	@Store.State("cities")
	public cities: Store.City.City[];

	@Store.State("meta")
	public meta: Meta;

	@Store.Action("init")
	public init: () => void;

	private weekdays: IWeekday[] = DateService.weekdays();

	public shops(
		params: pagination.IParams
	): Promise<pagination.Response<Store.Shop.Shop>> {
		if (this.meta.city) {
			return SmartStoreApi.shop.query({
				...params,
				cityId: parseInt(this.meta.city)
			});
		}
		return Promise.resolve({
			total: 0,
			records: []
		});
	}

	public mounted(): void {
		this.init();
	}

	public delivery(day: number): string {
		if (day !== undefined) {
			return this.weekdays.find(({ id }): boolean => {
				return id === day;
			}).title;
		}
		return "";
	}

	@Watch("meta.city")
	public update(): void {
		(this.$refs.table as RctTable).load();
	}
}
