import { Module } from "vuex";

import { PricesInitialState, PricesState } from "./prices.state";
import { PricesMutations } from "./prices.mutations";
import { ContactsOperations } from "./prices.operations";

export const PricesStore: Module<PricesState, {}> = {
	state: PricesInitialState,
	mutations: PricesMutations,
	actions: ContactsOperations,
	namespaced: true
};
