export interface PricesState {
	loading: boolean;
	prices: Store.Price.Prices;
}

export const PricesInitialState = (): PricesState => ({
	loading: false,
	prices: {
		deliveryPrices: new Array(7).fill(0),
		luxuryPrices: new Array(7).fill(0)
	}
});
