import { PricesState } from "./prices.state";

enum Types {
	LOADING = "[CONTACTS] LOADING",
	SET_PRICES = "[CONTACTS] SET_PRICES"
}

interface ContactsMutations {
	[key: string]: (state: PricesState, { loading }: PricesActions.All) => void;
}

export const PricesMutations: ContactsMutations = {
	[Types.LOADING]: (
		state: PricesState,
		{ loading }: PricesActions.All
	): void => {
		state.loading = loading;
	},
	[Types.SET_PRICES]: (
		state: PricesState,
		{ prices }: PricesActions.All
	): void => {
		state.prices = prices;
	}
};

export namespace PricesActions {
	export class Loading {
		public readonly type: Types = Types.LOADING;
		public constructor(public loading: boolean) {}
	}

	export class SetPrices {
		public readonly type: Types = Types.SET_PRICES;
		public constructor(public prices: Store.Price.Prices) {}
	}

	export type All = Loading & SetPrices;
}
