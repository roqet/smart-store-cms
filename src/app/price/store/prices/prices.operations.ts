import { ActionContext } from "vuex";
import { SmartStoreApi, Pages } from "@api/smartstore";
import { GlobalActions } from "@/common/store";
import { PricesState } from "./prices.state";
import { PricesActions } from "./prices.mutations";

export const ContactsOperations = {
	init: ({ commit }: ActionContext<PricesState, {}>): void => {
		Promise.resolve(commit(new PricesActions.Loading(true)))
			.then(
				(): Promise<Store.Price.Prices> => {
					return SmartStoreApi.price.get();
				}
			)
			.then((prices): void => {
				commit(new PricesActions.SetPrices(prices));
				commit(new PricesActions.Loading(false));
			})
			.catch((error): void => {
				const { errors, type, options } = new GlobalActions.Errors(
					error.errors
				);
				commit(type, errors, options);
				commit(new PricesActions.Loading(false));
			});
	},

	update: (
		{ commit }: ActionContext<PricesState, {}>,
		prices: Store.Price.Prices
	): void => {
		Promise.resolve(commit(new PricesActions.Loading(true)))
			.then(
				(): Promise<Store.Price.Prices> => {
					return SmartStoreApi.price.update(prices);
				}
			)
			.then(
				(): GlobalActions.Messages => {
					return new GlobalActions.Messages([
						{
							i18n: "messages.success.pricesUpdated"
						}
					]);
				}
			)
			.then(({ messages, type, options }): void => {
				commit(new PricesActions.Loading(false));
				commit(type, messages, options);
			})
			.catch((error): void => {
				const { errors, type, options } = new GlobalActions.Errors(
					error.errors
				);
				commit(type, errors, options);
				commit(new PricesActions.Loading(false));
			});
	}
};
