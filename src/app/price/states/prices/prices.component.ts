import { Component, Mixins } from "vue-property-decorator";
import { namespace } from "vuex-class";
import { UseGlobalStore } from "@/common/mixins";
import ContactsPageTpl from "./prices.component.vue";

const Store = namespace("prices");

@Component
export class PricesPage extends Mixins(ContactsPageTpl, UseGlobalStore) {
	@Store.State("loading")
	public loading: boolean;

	@Store.State("prices")
	public prices: Store.Price.Prices;

	@Store.Action("init")
	public init: () => void;

	public mounted(): void {
		this.init();
	}
}
