import { Component, Mixins } from "vue-property-decorator";
import { namespace } from "vuex-class";
import { UseGlobalStore } from "@/common/mixins";
import ManagePricesPageTpl from "./manage.component.vue";

const Store = namespace("prices");

@Component
export class ManagePricesPage extends Mixins(
	ManagePricesPageTpl,
	UseGlobalStore
) {
	@Store.State("loading")
	public loading: boolean;

	@Store.State("prices")
	public prices: Store.Price.Prices;

	@Store.Action("init")
	public init: () => void;

	@Store.Action("update")
	public update: (prices: Store.Price.Prices) => void;

	public save(): void {
		this.update(this.prices);
	}

	public cancel(): void {
		this.$router.push({ name: "contacts" });
	}

	public mounted(): void {
		this.init();
	}
}
