import { RouteConfig } from "vue-router";

import { PricesPage } from "../states/prices/prices.component";
import { ManagePricesPage } from "./../states/manage/manage.component";

export const PriceRoutes: RouteConfig[] = [
	{
		path: "/prices",
		name: "prices",
		component: PricesPage,
		meta: { root: "prices" }
	},
	{
		path: "/prices/edit",
		name: "editPrices",
		component: ManagePricesPage,
		meta: { root: "news" }
	}
];
