import { Shell } from "./shell/shell.component";
import { ShellService, NavigationConfig } from "./shell.service";

export class RctShellPlugin {
	public static install(vue, config: NavigationConfig): void {
		ShellService.config(config);
	}
}

export { Shell };
