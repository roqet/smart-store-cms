import { Route, RouteConfig } from "vue-router";

export interface SubmenuChildren {
	anchor: string;
	state: Partial<RouteConfig>;
}

export interface ISubmenu {
	show: string[];
	submenu: SubmenuChildren[];
}

interface Submenus {
	[key: string]: ISubmenu;
}

export interface Navigation {
	anchor: string;
	state: { name: string };
	icon?: string;
	children?: Navigation[];
	i?: number;
}

export interface NavigationConfig {
	navigation: Navigation[];
	submenus: Submenus;
}

export class ShellService {
	private static _navigation: Navigation[];
	private static _submenus: Submenus;

	public static config({
		navigation = [],
		submenus
	}: NavigationConfig): void {
		this._navigation = navigation.map(
			(item, i): Navigation => {
				return { ...item, i };
			}
		);
		this._submenus = submenus;
	}

	public static get navigation(): Navigation[] {
		return this._navigation;
	}

	public static get submenus(): Submenus {
		return this._submenus;
	}

	public static getRootNavigation(route: Route): Navigation {
		return this.navigation.filter((item): boolean => {
			return (
				item.state.name == route.matched[0].meta.root ||
				item.state.name == route.matched[0].name
			);
		})[0];
	}
}
