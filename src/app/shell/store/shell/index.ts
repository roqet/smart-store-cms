import { Module } from "vuex";

import { ShellState, ShellInitialState } from "./shell.state";
import { ShellMutations } from "./shell.mutations";
import * as ShellOperations from "./shell.operations";

export const ShellStore: Module<ShellState, {}> = {
	state: ShellInitialState,
	mutations: ShellMutations,
	actions: ShellOperations,
	namespaced: true
};
