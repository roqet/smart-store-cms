import { ShellState } from "./shell.state";

enum Types {
	LOADING = "[SHELL] LOADING",
	INITIALIZED = "[SHELL] INITIALIZED"
}

interface ShellMutations {
	[key: string]: (state: ShellState, { loading }: ShellActions.All) => void;
}

export const ShellMutations: ShellMutations = {
	[Types.LOADING]: (
		state: ShellState,
		{ loading }: ShellActions.All
	): void => {
		state.loading = loading;
	},
	[Types.INITIALIZED]: (
		state: ShellState,
		{ user }: ShellActions.All
	): void => {
		state.user = user;
	}
};

export namespace ShellActions {
	export class Loading {
		public readonly type: Types = Types.LOADING;
		public constructor(public loading: boolean) {}
	}

	export class Initialized {
		public readonly type: Types = Types.INITIALIZED;
		public constructor(public user: Store.DTO.User) {}
	}

	export type All = Loading & Initialized;
}
