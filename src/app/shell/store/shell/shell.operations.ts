import { ActionContext } from "vuex";
import { SmartStoreApi } from "@api/smartstore";
import { GlobalActions } from "@/common/store";
import { ShellState } from "./shell.state";
import { ShellActions } from "./shell.mutations";

export const init = ({ commit }: ActionContext<ShellState, {}>): void => {
	Promise.resolve(commit(new ShellActions.Loading(true)))
		.then(
			(): Promise<Store.DTO.User> => {
				return SmartStoreApi.user.current();
			}
		)
		.then((user): void => {
			commit(new ShellActions.Initialized(user));
			commit(new ShellActions.Loading(false));
		})
		.catch((error): void => {
			const { errors, type, options } = new GlobalActions.Errors(
				error.errors
			);
			commit(type, errors, options);
			commit(new ShellActions.Loading(false));
		});
};
