export interface ShellState {
	loading: boolean;
	user: Store.DTO.User;
}

export const ShellInitialState = (): ShellState => ({
	loading: false,
	user: null
});
