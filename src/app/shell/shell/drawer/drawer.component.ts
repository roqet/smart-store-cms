import { Component, Prop, Mixins } from "vue-property-decorator";
import { ShellService, Navigation } from "./../../shell.service";
import Template from "./drawer.component.vue";

@Component
export class ShellDrawer extends Mixins(Template) {
	@Prop() public toggle: () => void;

	public logo: string = require("@theme/smartstore/images/logo.png");

	public get navigation(): Navigation[] {
		return ShellService.navigation;
	}
}
