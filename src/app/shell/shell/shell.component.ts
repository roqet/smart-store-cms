import { Component, Mixins } from "vue-property-decorator";
import { namespace } from "vuex-class";
import { ShellHeader } from "./header/header.component";
import { ShellDrawer } from "./drawer/drawer.component";
import ShellTpl from "./shell.component.vue";
import "./shell.component.scss";

const Store = namespace("shell");

@Component({
	components: { ShellHeader, ShellDrawer }
})
export class Shell extends Mixins(ShellTpl) {
	@Store.State("loading")
	public loading: boolean;

	@Store.Action("init")
	public init: () => void;

	public drawer: boolean = false;

	public toggle(): void {
		this.drawer = !this.drawer;
	}

	public mounted(): void {
		this.init();
	}
}
