import { Component, Mixins, Prop } from "vue-property-decorator";
import { namespace } from "vuex-class";
import { ShellService, SubmenuChildren } from "./../../shell.service";
import Template from "./header.component.vue";

const Store = namespace("shell");

@Component
export class ShellHeader extends Mixins(Template) {
	@Prop() public toggle: () => void;

	@Store.State("user")
	public user: Store.DTO.User;

	public logout(): void {
		Promise.resolve(localStorage.removeItem("Store@Token")).then(
			(): void => {
				window.location.replace(`${window.location.origin}/login.html`);
			}
		);
	}

	public get submenu(): SubmenuChildren[] {
		return Object.keys(ShellService.submenus)
			.filter((key): boolean => {
				return ShellService.submenus[key].show.includes(
					this.$route.name
				);
			})
			.reduce((res, key): SubmenuChildren[] => {
				return ShellService.submenus[key].submenu;
			}, []);
	}
}
