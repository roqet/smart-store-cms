import { Component, Mixins } from "vue-property-decorator";
import { namespace } from "vuex-class";
import { UseGlobalStore } from "@/common/mixins";
import AboutPageTpl from "./about.component.vue";

const Store = namespace("about");

@Component
export class AboutPage extends Mixins(AboutPageTpl, UseGlobalStore) {
	@Store.State("loading")
	public loading: boolean;

	@Store.State("about")
	public about: Store.Pages.Contacts;

	@Store.Action("init")
	public init: () => void;

	public mounted(): void {
		this.init();
	}
}
