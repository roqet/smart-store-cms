import { Component, Mixins } from "vue-property-decorator";
import { namespace } from "vuex-class";
import { LocaleMessage } from "vue-i18n";
import { UseCkeditor, UseGlobalStore } from "@/common/mixins";
import ManageAboutPageTpl from "./manage.component.vue";

const Store = namespace("about");

@Component
export class ManageAboutPage extends Mixins(
	ManageAboutPageTpl,
	UseCkeditor,
	UseGlobalStore
) {
	@Store.State("loading")
	public loading: boolean;

	@Store.State("about")
	public about: Store.Pages.About;

	@Store.Action("init")
	public init: () => void;

	@Store.Action("update")
	public update: (about: Store.Pages.About) => void;

	public get title(): LocaleMessage {
		return this.$t("pages.aboutUsColonEdit");
	}

	public save(): void {
		this.update(this.about);
	}

	public cancel(): void {
		this.$router.push({ name: "home" });
	}

	public mounted(): void {
		this.init();
	}
}
