import { Module } from "vuex";

import { AboutState, AboutInitialState } from "./about.state";
import { ContactsMutations } from "./about.mutations";
import { ContactsOperations } from "./about.operations";

export const AboutStore: Module<AboutState, {}> = {
	state: AboutInitialState,
	mutations: ContactsMutations,
	actions: ContactsOperations,
	namespaced: true
};
