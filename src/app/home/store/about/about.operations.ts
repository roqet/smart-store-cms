import { ActionContext } from "vuex";
import { SmartStoreApi, Pages } from "@api/smartstore";
import { GlobalActions } from "@/common/store";
import { AboutState } from "./about.state";
import { ContactsActions } from "./about.mutations";

export const ContactsOperations = {
	init: ({ commit }: ActionContext<AboutState, {}>): void => {
		Promise.resolve(commit(new ContactsActions.Loading(true)))
			.then(
				(): Promise<Store.Pages.About> => {
					return SmartStoreApi.page.get(Pages.about);
				}
			)
			.then((contacts): void => {
				commit(new ContactsActions.SetAbout(contacts));
				commit(new ContactsActions.Loading(false));
			})
			.catch((error): void => {
				const { errors, type, options } = new GlobalActions.Errors(
					error.errors
				);
				commit(type, errors, options);
				commit(new ContactsActions.Loading(false));
			});
	},

	update: (
		{ commit }: ActionContext<AboutState, {}>,
		about: Store.Pages.About
	): void => {
		Promise.resolve(commit(new ContactsActions.Loading(true)))
			.then(
				(): Promise<Store.Pages.About> => {
					return SmartStoreApi.page.update<Store.Pages.About>(about);
				}
			)
			.then(
				(): GlobalActions.Messages => {
					return new GlobalActions.Messages([
						{
							i18n: "messages.success.aboutUpdated"
						}
					]);
				}
			)
			.then(({ messages, type, options }): void => {
				commit(new ContactsActions.Loading(false));
				commit(type, messages, options);
			})
			.catch((error): void => {
				const { errors, type, options } = new GlobalActions.Errors(
					error.errors
				);
				commit(type, errors, options);
				commit(new ContactsActions.Loading(false));
			});
	}
};
