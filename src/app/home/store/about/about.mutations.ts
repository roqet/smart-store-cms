import { AboutState } from "./about.state";

enum Types {
	LOADING = "[CONTACTS] LOADING",
	SET_CONTACTS = "[CONTACTS] SET_CONTACTS"
}

interface ContactsMutations {
	[key: string]: (
		state: AboutState,
		{ loading }: ContactsActions.All
	) => void;
}

export const ContactsMutations: ContactsMutations = {
	[Types.LOADING]: (
		state: AboutState,
		{ loading }: ContactsActions.All
	): void => {
		state.loading = loading;
	},
	[Types.SET_CONTACTS]: (
		state: AboutState,
		{ about }: ContactsActions.All
	): void => {
		state.about = about;
	}
};

export namespace ContactsActions {
	export class Loading {
		public readonly type: Types = Types.LOADING;
		public constructor(public loading: boolean) {}
	}

	export class SetAbout {
		public readonly type: Types = Types.SET_CONTACTS;
		public constructor(public about: Store.Pages.About) {}
	}

	export type All = Loading & SetAbout;
}
