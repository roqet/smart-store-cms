import { Pages } from "@api/smartstore";

export interface AboutState {
	loading: boolean;
	about: Store.Pages.About;
}

export const AboutInitialState = (): AboutState => ({
	loading: false,
	about: {
		type: Pages.contacts,
		content: ""
	}
});
