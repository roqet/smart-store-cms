import { RouteConfig } from "vue-router";

import { AboutPage } from "./../states/about/about.component";
import { ManageAboutPage } from "./../states/manage/manage.component";

export const HomeRoutes: RouteConfig[] = [
	{
		path: "/home",
		name: "home",
		component: AboutPage
	},
	{
		path: "/edit",
		name: "edit",
		component: ManageAboutPage
	}
];
