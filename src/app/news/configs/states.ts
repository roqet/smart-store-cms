import { RouteConfig } from "vue-router";

import { NewsPage } from "../states/news/news.component";
import { ManageNewsPage } from "./../states/manage/manage.component";

export const NewsRoutes: RouteConfig[] = [
	{
		path: "/news-and-promotions",
		name: "news",
		component: NewsPage,
		meta: { root: "news" }
	},
	{
		path: "/news-and-promotions/create",
		name: "createNews",
		component: ManageNewsPage,
		meta: { root: "news" }
	},
	{
		path: "/news-and-promotions/:id/edit",
		name: "editNews",
		component: ManageNewsPage,
		meta: { root: "news" }
	}
];
