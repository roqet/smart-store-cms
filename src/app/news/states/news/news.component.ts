import { Component, Mixins } from "vue-property-decorator";
import { LocaleMessage } from "vue-i18n";
import { SmartStoreApi } from "@api/smartstore";
import AllNewsPageTpl from "./news.component.vue";

@Component
export class NewsPage extends Mixins(AllNewsPageTpl) {
	public news(
		params: pagination.IParams
	): Promise<pagination.Response<Store.News.News>> {
		return SmartStoreApi.news.query(params, {
			id: true,
			type: true,
			title: true,
			content: true,
			createdAt: true,
			updatedAt: true
		} as any);
	}

	public type(type: string = ""): LocaleMessage {
		return this.$t(`forms.${type.toLowerCase()}`);
	}
}
