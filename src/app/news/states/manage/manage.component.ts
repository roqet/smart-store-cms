import { Component, Mixins } from "vue-property-decorator";
import { LocaleMessage } from "vue-i18n";
import { namespace } from "vuex-class";
import ManageShopPageTpl from "./manage.component.vue";
import { UseCkeditor, UseGlobalStore } from "@/common/mixins";

const Store = namespace("news");

interface Type {
	id: "NEWS" | "SALES";
	title: LocaleMessage;
}

@Component
export class ManageNewsPage extends Mixins(
	ManageShopPageTpl,
	UseCkeditor,
	UseGlobalStore
) {
	@Store.State("loading")
	public loading: boolean;

	@Store.State("news")
	public news: Store.News.News;

	@Store.Action("reset")
	private reset: () => void;

	@Store.Action("save")
	private saveNews: (news: Store.News.News) => void;

	@Store.Action("get")
	private getNews: (id: number) => void;

	public get types(): Type[] {
		return [
			{
				id: "NEWS",
				title: this.$t("forms.news")
			},
			{
				id: "SALES",
				title: this.$t("forms.promotion")
			}
		];
	}

	public get title(): LocaleMessage {
		if (this.$route.params.id) {
			return this.$t("pages.editNewsSlashPromotion");
		} else {
			return this.$t("pages.createNewsSlashPromotion");
		}
	}

	public save(): void {
		this.saveNews(this.news);
	}

	public cancel(): void {
		this.$router.push({ name: "news" });
	}

	public mounted(): void {
		((id: string): void => {
			if (id) this.getNews(parseInt(id));
		})(this.$route.params.id);
	}

	public destroyed(): void {
		this.reset();
	}
}
