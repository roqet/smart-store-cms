import { NewsState } from "./news.state";

interface NewsMutations {
	[key: string]: (state: NewsState, payload: NewsActions.All) => void;
}

enum Types {
	LOADING = "[NEWS] LOADING",
	RESET_NEWS = "[NEWS] RESET_NEWS",
	SET_NEWS = "[NEWS] SET_NEWS"
}

export const NewsMutations: NewsMutations = {
	[Types.LOADING]: (state: NewsState, { loading }: NewsActions.All): void => {
		state.loading = loading;
	},
	[Types.RESET_NEWS]: (state: NewsState): void => {
		state.news = {};
	},
	[Types.SET_NEWS]: (state: NewsState, { news }: NewsActions.All): void => {
		state.news = { ...news };
	}
};

export namespace NewsActions {
	export class Loading {
		public readonly type: Types = Types.LOADING;
		public constructor(public loading: boolean) {}
	}

	export class ResetNews {
		public readonly type: Types = Types.RESET_NEWS;
	}

	export class SetNews {
		public readonly type: Types = Types.SET_NEWS;
		public constructor(public news: Store.News.News) {}
	}

	export type All = Loading & ResetNews & SetNews;
}
