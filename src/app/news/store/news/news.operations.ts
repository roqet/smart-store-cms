import { ActionContext } from "vuex";
import { SmartStoreApi } from "@api/smartstore";
import { GlobalActions } from "@/common/store";
import { NewsState } from "./news.state";
import { NewsActions } from "./news.mutations";

export const NewsOperations = {
	reset: ({ commit }: ActionContext<NewsState, {}>): void => {
		commit(new NewsActions.ResetNews());
	},

	get: ({ commit }: ActionContext<NewsState, {}>, id: number): void => {
		Promise.resolve(commit(new NewsActions.Loading(true)))
			.then((): Promise<Store.News.News> => SmartStoreApi.news.get(id))
			.then((news): void => {
				commit(new NewsActions.SetNews(news));
				commit(new NewsActions.Loading(false));
			})
			.catch((error): void => {
				const { errors, type, options } = new GlobalActions.Errors(
					error.errors
				);
				commit(type, errors, options);
				commit(new NewsActions.Loading(false));
			});
	},

	save: (
		{ commit }: ActionContext<NewsState, {}>,
		news: Store.News.News
	): void => {
		Promise.resolve(commit(new NewsActions.Loading(true)))
			.then(
				(): Promise<Store.News.News> => {
					if (news.id) return SmartStoreApi.news.update(news);
					else return SmartStoreApi.news.create(news);
				}
			)
			.then(
				({ title }): GlobalActions.Messages => {
					return new GlobalActions.Messages([
						{
							i18n: `messages.success.news${
								news.id ? "Updated" : "Created"
							}`,
							params: { title }
						}
					]);
				}
			)
			.then(
				({ messages, type, options }: GlobalActions.Messages): void => {
					commit(new NewsActions.Loading(false));
					commit(type, messages, options);
					if (!news.id) commit(new NewsActions.ResetNews());
				}
			)
			.catch((error): void => {
				const { errors, type, options } = new GlobalActions.Errors(
					error.errors
				);
				commit(type, errors, options);
				commit(new NewsActions.Loading(false));
			});
	}
};
