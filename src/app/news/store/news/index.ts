import { Module } from "vuex";

import { NewsInitialState, NewsState } from "./news.state";
import { NewsMutations } from "./news.mutations";
import { NewsOperations } from "./news.operations";

export const NewsStore: Module<NewsState, {}> = {
	state: NewsInitialState,
	mutations: NewsMutations,
	actions: NewsOperations,
	namespaced: true
};
