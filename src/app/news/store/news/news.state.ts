export interface NewsState {
	loading: boolean;
	news: Partial<Store.News.News>;
}

export const NewsInitialState = (): NewsState => ({
	loading: false,
	news: {}
});
