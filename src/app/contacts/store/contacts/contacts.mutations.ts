import { ContactsState } from "./contacts.state";

enum Types {
	LOADING = "[CONTACTS] LOADING",
	SET_CONTACTS = "[CONTACTS] SET_CONTACTS"
}

interface ContactsMutations {
	[key: string]: (
		state: ContactsState,
		{ loading }: ContactsActions.All
	) => void;
}

export const ContactsMutations: ContactsMutations = {
	[Types.LOADING]: (
		state: ContactsState,
		{ loading }: ContactsActions.All
	): void => {
		state.loading = loading;
	},
	[Types.SET_CONTACTS]: (
		state: ContactsState,
		{ contacts }: ContactsActions.All
	): void => {
		state.contacts = contacts;
	}
};

export namespace ContactsActions {
	export class Loading {
		public readonly type: Types = Types.LOADING;
		public constructor(public loading: boolean) {}
	}

	export class SetContacts {
		public readonly type: Types = Types.SET_CONTACTS;
		public constructor(public contacts: Store.Pages.Contacts) {}
	}

	export type All = Loading & SetContacts;
}
