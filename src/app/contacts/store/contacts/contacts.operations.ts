import { ActionContext } from "vuex";
import { SmartStoreApi, Pages } from "@api/smartstore";
import { GlobalActions } from "@/common/store";
import { ContactsState } from "./contacts.state";
import { ContactsActions } from "./contacts.mutations";

export const ContactsOperations = {
	init: ({ commit }: ActionContext<ContactsState, {}>): void => {
		Promise.resolve(commit(new ContactsActions.Loading(true)))
			.then(
				(): Promise<Store.Pages.Contacts> => {
					return SmartStoreApi.page.get(Pages.contacts);
				}
			)
			.then((contacts): void => {
				commit(new ContactsActions.SetContacts(contacts));
				commit(new ContactsActions.Loading(false));
			})
			.catch((error): void => {
				const { errors, type, options } = new GlobalActions.Errors(
					error.errors
				);
				commit(type, errors, options);
				commit(new ContactsActions.Loading(false));
			});
	},

	update: (
		{ commit }: ActionContext<ContactsState, {}>,
		contacts: Store.Pages.Contacts
	): void => {
		Promise.resolve(commit(new ContactsActions.Loading(true)))
			.then(
				(): Promise<Store.Pages.Contacts> => {
					return SmartStoreApi.page.update<Store.Pages.Contacts>(
						contacts
					);
				}
			)
			.then(
				(): GlobalActions.Messages => {
					return new GlobalActions.Messages([
						{
							i18n: "messages.success.contactsUpdated"
						}
					]);
				}
			)
			.then(({ messages, type, options }): void => {
				commit(new ContactsActions.Loading(false));
				commit(type, messages, options);
			})
			.catch((error): void => {
				const { errors, type, options } = new GlobalActions.Errors(
					error.errors
				);
				commit(type, errors, options);
				commit(new ContactsActions.Loading(false));
			});
	}
};
