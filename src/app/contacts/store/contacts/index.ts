import { Module } from "vuex";

import { ContactsInitialState, ContactsState } from "./contacts.state";
import { ContactsMutations } from "./contacts.mutations";
import { ContactsOperations } from "./contacts.operations";

export const ContactsStore: Module<ContactsState, {}> = {
	state: ContactsInitialState,
	mutations: ContactsMutations,
	actions: ContactsOperations,
	namespaced: true
};
