import { Pages } from "@api/smartstore";

export interface ContactsState {
	loading: boolean;
	contacts: Store.Pages.Contacts;
}

export const ContactsInitialState = (): ContactsState => ({
	loading: false,
	contacts: {
		type: Pages.contacts,
		content: "",
		meta: {
			facebook: "",
			telegram: "",
			instagram: "",
			info: "",
			hr: "",
			tel: ""
		}
	}
});
