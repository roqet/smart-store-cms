import { Component, Mixins } from "vue-property-decorator";
import { LocaleMessage } from "vue-i18n";
import { namespace } from "vuex-class";
import ManageContactsPageTpl from "./manage.component.vue";
import { UseCkeditor, UseGlobalStore } from "@/common/mixins";

const Store = namespace("contacts");

@Component
export class ManageContactsPage extends Mixins(
	ManageContactsPageTpl,
	UseCkeditor,
	UseGlobalStore
) {
	@Store.State("loading")
	public loading: boolean;

	@Store.State("contacts")
	public contacts: Store.Pages.Contacts;

	@Store.Action("init")
	public init: () => void;

	@Store.Action("update")
	public update: (contacts: Store.Pages.Contacts) => void;

	public get title(): LocaleMessage {
		return this.$t("pages.contactsColonEdit");
	}

	public save(): void {
		this.update(this.contacts);
	}

	public cancel(): void {
		this.$router.push({ name: "contacts" });
	}

	public mounted(): void {
		this.init();
	}
}
