import { Component, Mixins } from "vue-property-decorator";
import { namespace } from "vuex-class";
import { UseGlobalStore } from "@/common/mixins";
import ContactsPageTpl from "./contacts.component.vue";

const Store = namespace("contacts");

@Component
export class ContactsPage extends Mixins(ContactsPageTpl, UseGlobalStore) {
	@Store.State("loading")
	public loading: boolean;

	@Store.State("contacts")
	public contacts: Store.Pages.Contacts;

	@Store.Action("init")
	public init: () => void;

	public mounted(): void {
		this.init();
	}
}
