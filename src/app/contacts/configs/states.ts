import { RouteConfig } from "vue-router";

import { ContactsPage } from "../states/contacts/contacts.component";
import { ManageContactsPage } from "../states/manage/manage.component";

export const ContactsRoutes: RouteConfig[] = [
	{
		path: "/contacts",
		name: "contacts",
		component: ContactsPage
	},
	{
		path: "/contacts/edit",
		name: "editContacts",
		component: ManageContactsPage
	}
];
