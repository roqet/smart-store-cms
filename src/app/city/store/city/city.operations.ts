import { ActionContext } from "vuex";
import { SmartStoreApi } from "@api/smartstore";
import { GlobalActions } from "@/common/store";
import { CityState } from "./city.state";
import { CityActions } from "./city.mutations";

const setName = ({ commit }, name: string): void => {
	commit(new CityActions.SetName(name));
};

const save = (
	{ commit }: ActionContext<CityState, {}>,
	city: Store.City.CityInput
): void => {
	Promise.resolve(commit(new CityActions.Loading(true)))
		.then(
			(): Promise<Store.City.City> => {
				return SmartStoreApi.city.create(city);
			}
		)
		.then(
			(): GlobalActions.Messages => {
				return new GlobalActions.Messages([
					{
						i18n: "messages.success.cityAdded",
						params: { name: city.name }
					}
				]);
			}
		)
		.then(({ messages, type, options }): void => {
			commit(new CityActions.Loading(false));
			commit(new CityActions.ResetCity());
			commit(type, messages, options);
		})
		.catch((error): void => {
			const { errors, type, options } = new GlobalActions.Errors(
				error.errors
			);
			commit(type, errors, options);
			commit(new CityActions.Loading(false));
		});
};

export const CityOperations = { setName, save };
