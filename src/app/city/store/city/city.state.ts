export interface CityState {
	inprogress: boolean;
	city: Partial<Store.City.CityInput>;
	errors: string[];
	messages: store.state.Message[];
}

export const CityInitialState = (): CityState => ({
	inprogress: false,
	city: {},
	errors: [],
	messages: []
});
