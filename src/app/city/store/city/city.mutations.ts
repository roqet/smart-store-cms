import { CityState, CityInitialState } from "./city.state";

enum Types {
	LOADING = "[CITY] LOADING",
	RESET_CITY = "[CITY] RESET_CITY",
	SET_NAME = "[CITY] SET_NAME"
}

export const CityMutations = {
	[Types.RESET_CITY]: (state: CityState): void => {
		state.city = CityInitialState().city;
	},
	[Types.LOADING]: (state: CityState, { loading }: CityActions.All): void => {
		state.inprogress = loading;
	},
	[Types.SET_NAME]: (state: CityState, { name }: CityActions.All): void => {
		state.city.name = name;
	}
};

export namespace CityActions {
	export class Loading {
		public readonly type: Types = Types.LOADING;
		public constructor(public loading: boolean) {}
	}

	export class SetName {
		public readonly type: Types = Types.SET_NAME;
		public constructor(public name: string) {}
	}

	export class ResetCity {
		public readonly type: Types = Types.RESET_CITY;
	}

	export type All = Loading & SetName & ResetCity;
}
