import { Module } from "vuex";

import { CityState, CityInitialState } from "./city.state";
import { CityMutations } from "./city.mutations";
import { CityOperations } from "./city.operations";

export const CityStore: Module<CityState, {}> = {
	state: CityInitialState,
	mutations: CityMutations,
	actions: CityOperations,
	namespaced: true
};
