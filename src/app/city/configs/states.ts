import { RouteConfig } from "vue-router";

import { CitiesPage } from "./../states/cities/cities.component";
import { ManageCityPage } from "./../states/manage/manage.component";

export const CityRoutes: RouteConfig[] = [
	{
		path: "/cities",
		name: "cities",
		component: CitiesPage,
		meta: { root: "cities" }
	},
	{
		path: "/cities/add",
		name: "addCity",
		component: ManageCityPage,
		meta: { root: "shops" }
	}
];
