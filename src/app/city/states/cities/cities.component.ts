import { Component, Mixins } from "vue-property-decorator";
import { SmartStoreApi } from "@api/smartstore";
import CitiesPageTpl from "./cities.component.vue";

@Component
export class CitiesPage extends Mixins(CitiesPageTpl) {
	public cities(
		params: pagination.IParams
	): Promise<pagination.Response<Store.City.City>> {
		return SmartStoreApi.city.query(params);
	}
}
