import { Component, Mixins } from "vue-property-decorator";
import { namespace } from "vuex-class";
import { SmartStoreApi } from "@api/smartstore";
import { UseGlobalStore } from "@/common/mixins";
import ManageCityPageTpl from "./manage.component.vue";

const Store = namespace("city");

@Component
export class ManageCityPage extends Mixins(ManageCityPageTpl, UseGlobalStore) {
	@Store.State("inprogress")
	public inprogress: boolean;

	@Store.State("city")
	public city: Store.City.CityInput;

	@Store.State("messages")
	private messages: store.state.Message[];

	@Store.Action("setName")
	private setName: (name: string) => void;

	@Store.Action("save")
	private saveCity: (city: Store.City.CityInput) => void;

	public cities: Store.ApiDTO.GoogleCity[] = [];

	public search(q: string): Promise<Store.ApiDTO.GoogleCity[]> {
		return SmartStoreApi.city
			.find(q)
			.then((cities): Store.ApiDTO.GoogleCity[] => {
				return (this.cities = cities);
			});
	}

	public change(value: string): void {
		this.setName(
			this.cities.find(({ id }): boolean => {
				return id === value;
			}).name
		);
	}

	public save(): void {
		this.saveCity(this.city);
	}

	public cancel(): void {
		this.$router.push({ name: "cities" });
	}
}
