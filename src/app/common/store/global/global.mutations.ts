import { GlobalState } from "./global.state";
import { CommitOptions } from "vuex";

enum Types {
	ERRORS = "[GLOBAL] ERRORS",
	MESSAGES = "[GLOBAL] MESSAGES"
}

interface GlobalMutations {
	[key: string]: (state, payload) => void;
}

export const globalMutations: GlobalMutations = {
	[Types.ERRORS]: (state: GlobalState, errors: string[]): void => {
		state.errorMessages = errors;
	},
	[Types.MESSAGES]: (
		state: GlobalState,
		messages: store.state.Message[]
	): void => {
		state.successMessages = messages;
	}
};

export namespace GlobalActions {
	export class Errors {
		public readonly type: string = `global/${Types.ERRORS}`;
		public readonly options: CommitOptions = { root: true };
		public constructor(public errors: string[]) {}
	}

	export class Messages {
		public readonly type: string = `global/${Types.MESSAGES}`;
		public readonly options: CommitOptions = { root: true };
		public constructor(public messages: store.state.Message[]) {}
	}

	export type All = Errors & Messages;
}
