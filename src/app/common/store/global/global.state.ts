export interface GlobalState {
	errorMessages: string[];
	successMessages: store.state.Message[];
}

export const globalState = (): GlobalState => ({
	errorMessages: [],
	successMessages: []
});
