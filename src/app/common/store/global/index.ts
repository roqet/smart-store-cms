import { Module } from "vuex";

import { globalState, GlobalState } from "./global.state";
import { globalMutations, GlobalActions } from "./global.mutations";
import { GlobalOperations } from "./global.operations";

export const GlobalStore: Module<GlobalState, {}> = {
	state: globalState,
	mutations: globalMutations,
	actions: GlobalOperations,
	namespaced: true
};

export { GlobalActions };
