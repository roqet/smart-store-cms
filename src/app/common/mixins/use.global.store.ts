import { Component, Mixins, Watch } from "vue-property-decorator";
import { namespace } from "vuex-class";

const Store = namespace("global");

@Component
export class UseGlobalStore extends Mixins() {
	@Store.State("successMessages")
	public successMessages: store.state.Message[];

	@Store.State("errorMessages")
	public errorMessages: string[];

	@Watch("successMessages")
	public showMessages(value: store.state.Message[]): void {
		value.forEach(({ i18n, params }: store.state.Message): void => {
			this.$toast.success(this.$i18n.t(i18n, params));
		});
	}

	@Watch("errorMessages")
	public showErrors(value: string[]): void {
		value.forEach((error): void => {
			this.$toast.error(error);
		});
	}
}
