import { AxiosResponse } from "axios";
import { jsonToGraphQLQuery } from "json-to-graphql-query";
import { News } from "./news.queries";
import { Http } from "../graph";

export class NewsResource {
	public query(
		params: pagination.IParams,
		fields = News
	): Promise<pagination.Response<Store.News.News>> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				query: {
					getAllNews: {
						total: true,
						records: fields,
						__args: params
					}
				}
			})
		}).then(
			({ data }: AxiosResponse): pagination.Response<Store.News.News> =>
				data.getAllNews
		);
	}
	public get(id: number, fields = News): Promise<Store.News.News> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				query: {
					getNews: {
						...fields,
						__args: { id }
					}
				}
			})
		}).then(({ data }: AxiosResponse): Store.News.News => data.getNews);
	}

	public create(
		news: Store.News.News,
		fields = News
	): Promise<Store.News.News> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				mutation: {
					createNews: {
						...fields,
						__args: { news }
					}
				}
			})
		}).then(({ data }: AxiosResponse): Store.News.News => data.createNews);
	}

	public update(
		news: Store.News.News,
		fields = News
	): Promise<Store.News.News> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				mutation: {
					updateNews: {
						...fields,
						__args: { news }
					}
				}
			})
		}).then(({ data }: AxiosResponse): Store.News.News => data.updateNews);
	}
}
