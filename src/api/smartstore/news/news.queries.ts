import { Cloudinary } from "../queries";

export const News = {
	id: true,
	type: true,
	image: Cloudinary,
	title: true,
	content: true
};
