import { AxiosResponse } from "axios";
import { jsonToGraphQLQuery } from "json-to-graphql-query";
import { Shop } from "./shop.queries";
import { Http } from "../graph";

interface MarkerQueryParams extends pagination.IParams {
	cityId: number;
}

export class ShopResource {
	public query(
		params: MarkerQueryParams,
		fields = Shop
	): Promise<pagination.Response<Store.Shop.Shop>> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				query: {
					getMarkets: {
						total: true,
						records: fields,
						__args: params
					}
				}
			})
		}).then(
			({ data }: AxiosResponse): pagination.Response<Store.Shop.Shop> =>
				data.getMarkets
		);
	}

	public get(marketId: number, fields = Shop): Promise<Store.Shop.Shop> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				query: {
					getMarketWithDetails: {
						...fields,
						__args: { marketId }
					}
				}
			})
		}).then(
			({ data }: AxiosResponse): Store.Shop.Shop =>
				data.getMarketWithDetails
		);
	}

	public create(
		market: Store.Shop.Shop,
		fields = Shop
	): Promise<Store.Shop.Shop> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				mutation: {
					createMarket: {
						...fields,
						__args: { market }
					}
				}
			})
		}).then(
			({ data }: AxiosResponse): Store.Shop.Shop => data.createMarket
		);
	}

	public update(
		market: Store.Shop.Shop,
		fields = Shop
	): Promise<Store.Shop.Shop> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				mutation: {
					updateMarket: {
						...fields,
						__args: { market }
					}
				}
			})
		}).then(
			({ data }: AxiosResponse): Store.Shop.Shop => data.updateMarket
		);
	}
}
