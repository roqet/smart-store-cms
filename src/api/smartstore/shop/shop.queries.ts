export const Shop = {
	id: true,
	dayOfDelivery: true,
	deliveryLux: true,
	workingSchedule: {
		dayNum: true,
		start: true,
		end: true,
		isClosed: true
	},
	address: {
		id: true,
		name: true,
		cityId: true
	}
};
