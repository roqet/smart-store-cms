import { AxiosResponse } from "axios";
import { jsonToGraphQLQuery } from "json-to-graphql-query";
import { User } from "./user.queries";
import { Http } from "../graph";

export class UserResource {
	public current(fields = User): Promise<Store.DTO.User> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				query: {
					getCurrentUser: {
						...fields
					}
				}
			})
		}).then(
			({ data }: AxiosResponse): Store.DTO.User => data.getCurrentUser
		);
	}
}
