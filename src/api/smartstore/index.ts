import { CityResource } from "./city/city.resource";
import { NewsResource } from "./news/news.resource";
import { ShopResource } from "./shop/shop.resource";
import { PageResource } from "./page/page.resource";
import { PriceResource } from "./price/price.resource";
import { AuthResource } from "./auth/auth.resource";
import { UserResource } from "./user/user.resource";
import { Pages } from "./page/page.queries";

export class SmartStoreApi {
	public static city: CityResource = new CityResource();
	public static news: NewsResource = new NewsResource();
	public static shop: ShopResource = new ShopResource();
	public static page: PageResource = new PageResource();
	public static price: PriceResource = new PriceResource();
	public static user: UserResource = new UserResource();
}

export const AuthApi = new AuthResource();

export { Pages };
