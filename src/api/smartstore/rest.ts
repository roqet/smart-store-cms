import Axios, { AxiosInstance } from "axios";

const Rest: AxiosInstance = Axios.create({
	baseURL: `${process.env.API_BASE_URL}/api/rest`,
	withCredentials: true
});

export { Rest };
