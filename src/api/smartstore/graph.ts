import merge from "lodash/merge";
import Axios, {
	AxiosInstance,
	AxiosResponse,
	AxiosError,
	AxiosRequestConfig
} from "axios";

const Http: AxiosInstance = Axios.create({
	baseURL: `${process.env.API_BASE_URL}/graphql`,
	withCredentials: true
});

const errorHandler = ({
	response: { status, data }
}: AxiosError): Promise<string[]> => {
	if (status === 403) {
		return Promise.reject(
			window.location.replace(`${window.location.origin}/login.html`)
		);
	}
	return Promise.reject({ errors: [data] });
};

const responseHandler = ({ data }: AxiosResponse): Promise<AxiosResponse> => {
	if (data.errors) {
		return Promise.reject({
			errors: data.errors.map(({ message }): string => {
				return message;
			})
		});
	}
	return Promise.resolve(data);
};

const requestHandler = (config: AxiosRequestConfig): AxiosRequestConfig => {
	return merge({}, config, {
		headers: {
			Authorization: `Bearer ${localStorage.getItem("Store@Token")}`
		}
	});
};

Http.interceptors.response.use(responseHandler, errorHandler);
Http.interceptors.request.use(requestHandler);

export { Http };
