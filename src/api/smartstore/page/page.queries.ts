const Base = {
	type: true,
	content: true
};

export enum Pages {
	contacts = "CONTACTS",
	about = "ABOUT"
}

export const Page = {
	[Pages.contacts]: {
		...Base,
		meta: {
			facebook: true,
			telegram: true,
			instagram: true,
			info: true,
			hr: true,
			tel: true
		}
	},
	[Pages.about]: {
		...Base
	}
};
