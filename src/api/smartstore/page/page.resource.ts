import { AxiosResponse } from "axios";
import { jsonToGraphQLQuery } from "json-to-graphql-query";
import { Page, Pages } from "./page.queries";
import { Http } from "../graph";

interface MarkerQueryParams extends pagination.IParams {
	cityId: number;
}

export class PageResource {
	public get<T>(type: Pages, fields?): Promise<T> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				query: {
					getPage: {
						...(fields ? fields : Page[type]),
						__args: { type }
					}
				}
			})
		}).then(({ data }: AxiosResponse): T => data.getPage);
	}

	public update<T = {}>(page: { type: string } & T, fields?): Promise<T> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				mutation: {
					updatePage: {
						...(fields ? fields : Page[page.type]),
						__args: { page }
					}
				}
			})
		}).then(({ data }: AxiosResponse): T => data.updatePage);
	}
}
