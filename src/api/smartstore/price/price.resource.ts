import { AxiosResponse } from "axios";
import { jsonToGraphQLQuery } from "json-to-graphql-query";
import { Prices } from "./price.queries";
import { Http } from "../graph";

export class PriceResource {
	public get(fields = Prices): Promise<Store.Price.Prices> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				query: {
					getPrices: {
						...fields
					}
				}
			})
		}).then(
			({ data }: AxiosResponse): Store.Price.Prices => data.getPrices
		);
	}

	public update(
		prices: Store.Price.Prices,
		fields = Prices
	): Promise<Store.Price.Prices> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				mutation: {
					savePrices: {
						...fields,
						__args: { prices }
					}
				}
			})
		}).then(
			({ data }: AxiosResponse): Store.Price.Prices => data.savePrices
		);
	}
}
