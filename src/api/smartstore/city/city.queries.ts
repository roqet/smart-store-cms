export const City = {
	id: true,
	name: true,
	geometry: {
		latitude: true,
		longitude: true
	}
};

export const FindCity = {
	id: true,
	name: true
};

export const Address = {
	id: true,
	name: true,
	cityId: true
};
