import { AxiosResponse } from "axios";
import { jsonToGraphQLQuery } from "json-to-graphql-query";
import { City, FindCity, Address } from "./city.queries";
import { Http } from "../graph";
import { store } from "@configs/all";

export class CityResource {
	public find(
		input: string,
		fields = FindCity
	): Promise<Store.ApiDTO.GoogleCity[]> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				query: {
					findCities: {
						...fields,
						__args: { input }
					}
				}
			})
		}).then(
			({ data }: AxiosResponse): Store.ApiDTO.GoogleCity[] =>
				data.findCities
		);
	}

	public query(
		params: pagination.IParams,
		fields = City
	): Promise<pagination.Response<Store.City.City>> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				query: {
					getCities: {
						total: true,
						records: fields,
						__args: { ...params }
					}
				}
			})
		}).then(
			({ data }: AxiosResponse): pagination.Response<Store.City.City> =>
				data.getCities
		);
	}

	public create(
		{ name, placeId }: Store.City.CityInput,
		fields = City
	): Promise<Store.City.City> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				mutation: {
					saveCity: {
						...fields,
						__args: { name, placeId }
					}
				}
			})
		}).then(({ data }: AxiosResponse): Store.City.City => data.saveCity);
	}

	public address(
		params: Store.City.CityAddressParams,
		fields = Address
	): Promise<store.location.Address[]> {
		return Http.post("", {
			query: jsonToGraphQLQuery({
				query: {
					findAddresses: {
						...fields,
						__args: { ...params }
					}
				}
			})
		}).then(
			({ data }: AxiosResponse): store.location.Address[] =>
				data.findAddresses
		);
	}
}
