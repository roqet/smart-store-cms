export const Cloudinary = {
	bytes: true,
	// eslint-disable-next-line @typescript-eslint/camelcase
	created_at: true,
	etag: true,
	format: true,
	height: true,
	// eslint-disable-next-line @typescript-eslint/camelcase
	original_filename: true,
	placeholder: true,
	// eslint-disable-next-line @typescript-eslint/camelcase
	public_id: true,
	// eslint-disable-next-line @typescript-eslint/camelcase
	resource_type: true,
	// eslint-disable-next-line @typescript-eslint/camelcase
	secure_url: true,
	signature: true,
	type: true,
	url: true,
	version: true,
	width: true,
	tags: true
};
