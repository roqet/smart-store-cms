import { Rest } from "../rest";

interface LoginProps {
	phoneNumber: string;
	password: string;
}

export class AuthResource {
	public sendPassword(phone: string): Promise<string> {
		return Rest.post("/sendPassword", phone).then((): string => phone);
	}

	public login(creds: LoginProps): Promise<string> {
		return Rest.post("/login", creds).then(({ data }): string => data);
	}
}
