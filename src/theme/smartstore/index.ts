import { Component } from "vue";
import kebabCase from "lodash/kebabCase";
import * as Components from "./components";

import "./icons/icon.font";
import "./styles/main.scss";

interface LoopComponent {
	component: Component;
	name: string;
}

export class SmartStoreThemePlugin {
	public static install(Vue): void {
		Object.keys(Components)
			.map(
				(key): LoopComponent => {
					return { component: Components[key], name: kebabCase(key) };
				}
			)
			.forEach(({ component, name }): void => {
				Vue.component(name, component);
			});
	}
}
