module.exports = {
	files: ["./*.svg"],
	fontName: "smart-store",
	classPrefix: "si-",
	baseSelector: ".si",
	types: ["eot", "woff", "woff2", "ttf", "svg"]
};
