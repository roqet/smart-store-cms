import { Component, Mixins, Prop } from "vue-property-decorator";
import SiIconTpl from "./icon.component.vue";

interface Istyle {
	"font-size": string;
}

@Component
export class SiIcon extends Mixins(SiIconTpl) {
	@Prop({ required: true }) private name: string;
	@Prop({ default: 14 }) private size: number;

	public get icon(): string {
		return `si-${this.name}`;
	}

	public get style(): Istyle {
		return {
			"font-size": `${this.size}px`
		};
	}
}
