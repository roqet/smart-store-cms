export interface LoginState {
	loading: boolean;
	passwordSend: boolean;
	creds: {
		phoneNumber: string;
		password: string;
	};
}

export const LoginInitialState = (): LoginState => ({
	loading: false,
	passwordSend: false,
	creds: {
		phoneNumber: "+380",
		password: undefined
	}
});
