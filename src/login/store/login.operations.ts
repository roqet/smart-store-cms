import { ActionContext } from "vuex";
import { AuthApi } from "@api/smartstore";
import { LoginState } from "./login.state";
import { LoginActions } from "./login.mutations";

console.log(window.location);

export const sendPassword = (
	{ commit }: ActionContext<LoginState, {}>,
	phone: string
): void => {
	Promise.resolve(commit(new LoginActions.Loading(true)))
		.then(
			(): Promise<string> => {
				return AuthApi.sendPassword(phone);
			}
		)
		.then((phone): void => {
			commit(new LoginActions.PasswordSend(phone));
			commit(new LoginActions.Loading(false));
		});
};

export const login = (
	{ commit }: ActionContext<LoginState, {}>,
	creds: { phoneNumber: string; password: string }
): void => {
	Promise.resolve(commit(new LoginActions.Loading(true)))
		.then(
			(): Promise<string> => {
				return AuthApi.login(creds);
			}
		)
		.then(
			(token): Promise<void> => {
				return Promise.resolve(
					localStorage.setItem("Store@Token", token)
				);
			}
		)
		.then((): void => {
			window.location.replace(window.location.origin);
		});
};
