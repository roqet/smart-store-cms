import Vue from "vue";
import { Module } from "vuex";
import Vuex, { Store } from "vuex";

import { LoginInitialState, LoginState } from "./login.state";
import { LoginMutations } from "./login.mutations";
import * as LoginOperations from "./login.operations";

Vue.use(Vuex);

const LoginStore: Module<LoginState, {}> = {
	state: LoginInitialState,
	mutations: LoginMutations,
	actions: LoginOperations,
	namespaced: true
};

export const store = new Store({
	modules: {
		login: LoginStore
	}
});
