import { LoginState } from "./login.state";

interface LoginMutations {
	[key: string]: (state: LoginState, payload: LoginActions.All) => void;
}

enum Types {
	LOADING = "[NEWS] LOADING",
	PASSWORD_SEND = "[NEWS] PASSWORD_SEND"
}

export const LoginMutations: LoginMutations = {
	[Types.LOADING]: (
		state: LoginState,
		{ loading }: LoginActions.All
	): void => {
		state.loading = loading;
	},
	[Types.PASSWORD_SEND]: (
		state: LoginState,
		{ phoneNumber }: LoginActions.All
	): void => {
		state.passwordSend = true;
		state.creds.phoneNumber = phoneNumber;
	}
};

export namespace LoginActions {
	export class Loading {
		public readonly type: Types = Types.LOADING;
		public constructor(public loading: boolean) {}
	}

	export class PasswordSend {
		public readonly type: Types = Types.PASSWORD_SEND;
		public constructor(public phoneNumber: string) {}
	}

	export type All = Loading & PasswordSend;
}
