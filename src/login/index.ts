import Vue from "vue";
import VueMaterial from "vue-material";

Vue.use(VueMaterial);

export * from "./login";
export * from "./store";
