import { Component, Mixins } from "vue-property-decorator";
import { namespace } from "vuex-class";
import LoginTpl from "./login.vue";
import { RctLoaderOverlay } from "@modules/ui/loader-overlay/loader.overlay.component";

interface Creds {
	phoneNumber: string;
	password: string;
}

const Store = namespace("login");

@Component({
	components: {
		"rct-loader-overlay": RctLoaderOverlay
	}
})
export class Login extends Mixins(LoginTpl) {
	@Store.State("loading")
	public loading: boolean;

	@Store.State("creds")
	public creds: Creds;

	@Store.State("passwordSend")
	public passwordSend: boolean;

	@Store.Action("sendPassword")
	public sendPassword: (phone: string) => void;

	@Store.Action("login")
	public logIn: (creds: Creds) => void;

	public logo: string = require("@theme/smartstore/images/logo.png");

	public send(): void {
		this.sendPassword(this.creds.phoneNumber);
	}

	public login(): void {
		this.logIn(this.creds);
	}
}
