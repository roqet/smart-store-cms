declare namespace Store.City {
	interface City {
		id: number;
		name: string;
		geometry: {
			latitude: string;
			longitude: string;
		};
	}

	interface CityInput {
		name: string;
		placeId: string;
	}

	interface CityAddress {
		id: string;
		name: string;
		cityId: string;
	}

	interface CityAddressParams {
		input: string;
		geometry: {
			latitude: string;
			longitude: string;
		};
	}
}
