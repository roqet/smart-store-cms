declare namespace Store.ApiDTO {
	interface GoogleCity {
		id: string;
		name: string;
	}
}
