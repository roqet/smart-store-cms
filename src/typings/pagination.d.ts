declare namespace pagination {
	interface IParams {
		page: number;
		count: number;
	}

	interface Response<T> {
		total: number;
		records: T[];
	}
}
