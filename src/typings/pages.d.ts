declare namespace Store.Pages {
	interface BasePage {
		type: string;
		content: string;
	}

	interface Contacts extends BasePage {
		meta: {
			facebook: string;
			telegram: string;
			instagram: string;
			info: string;
			hr: string;
			tel: string;
		};
	}

	type About = BasePage;
}
