declare namespace Store.Price {
	interface Prices {
		deliveryPrices: number[];
		luxuryPrices: number[];
	}
}
