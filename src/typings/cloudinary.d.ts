declare namespace Cloud {
	interface Image {
		bytes: number;
		created_at: string;
		etag: string;
		format: string;
		height: number;
		original_filename: string;
		placeholder: boolean;
		public_id: string;
		resource_type: string;
		secure_url: string;
		signature: string;
		tags: [string];
		type: string;
		url: string;
		version: number;
		width: number;
	}
}
