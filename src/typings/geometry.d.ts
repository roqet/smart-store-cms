declare namespace store.location {
	interface Geometry {
		page: number;
		count: number;
	}

	interface Address {
		id: string;
		name: string;
		cityId: string;
	}

	interface AddressParams {
		input: string;
		geometry: {
			latitude: string;
			longitude: string;
		};
	}
}
