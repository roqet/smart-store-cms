declare namespace Store.Shop {
	interface WorkingSchedule {
		dayNum: number;
		start: string;
		end: string;
		isClosed: boolean;
	}

	interface Address {
		id: string;
		name: string;
		cityId: string;
	}

	interface Shop {
		id: number;
		dayOfDelivery: number;
		deliveryLux: number;
		workingSchedule: WorkingSchedule[];
		address: Address;
	}
}
