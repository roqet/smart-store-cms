declare namespace Store.DTO {
	interface User {
		id: number;
		phoneNumber: string;
		firstName: string;
		lastName: string;
		dateOfBirth: number;
		sex: 0 | 1;
		cardNumber: string;
		email: string;
	}
}
