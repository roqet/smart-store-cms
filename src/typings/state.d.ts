declare namespace store.state {
	interface Message {
		i18n: string;
		params?: {
			[key: string]: string;
		};
		link?: string;
	}
}
