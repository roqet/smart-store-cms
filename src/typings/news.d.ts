declare namespace Store.News {
	interface News {
		id?: number;
		type: string;
		image: Cloud.Image;
		title: string;
		content: string;
		createdAt?: number;
		updatedAt?: number;
	}
}
