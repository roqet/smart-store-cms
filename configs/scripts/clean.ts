import rimraf from "rimraf";
import path from "path";

const dir = path.resolve(process.cwd(), "./dist");

rimraf.sync(dir)