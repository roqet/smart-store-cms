import merge from "webpack-merge";
import { common } from "./common";

export const production = merge(common, {
	env: {
		CLOUDINARY_CLOUD_NAME: "dqbghkwrj",
		CLOUDINARY_UPLOAD_URL:
			"https://api.cloudinary.com/v1_1/dqbghkwrj/image/upload",
		CLOUDINARY_UPLOAD_PRESET: "xgihrkhc"
	}
});
