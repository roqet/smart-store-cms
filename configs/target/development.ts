import merge from "webpack-merge";
import { common } from "./common";

export const development = merge(common, {
	env: {
		CLOUDINARY_CLOUD_NAME: "donumx4p6",
		CLOUDINARY_UPLOAD_URL:
			"https://api.cloudinary.com/v1_1/donumx4p6/image/upload",
		CLOUDINARY_UPLOAD_PRESET: "bhjslwrd"
	}
});
