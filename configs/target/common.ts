import path from "path";

export const common = {
	output: {
		path: path.resolve(process.cwd(), "./dist"),
		publicPath: "/"
	},
	env: {
		API_BASE_URL: ""
	}
};
